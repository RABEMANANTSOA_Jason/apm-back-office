<?php namespace App\Controllers;

use App\Models\DocumentModel;

class Document extends BaseController
{
    private $path = '/images/documents/';
    private $relativepath = './images/documents';

    public function index()
    {
        $data = [];
        $model = new DocumentModel();
        $data['documents'] = $model->findAll();
        return view('documents/index',$data);
    }

    public function detail($id = NULL){
        $data = [];
        $model = new DocumentModel();
        $data['documents'] = $model->find($id);
        if($data['documents']){
            return view('documents/detail',$data);
        }
        else{
            $session = session();
			$session->setFlashData('error', "Document non trouvé");
            return redirect()->to("/documents/index");
        }
    }

    public function add()
    {
        $data = [];
        helper(['form']);
        $model = new DocumentModel();        
        if (!$this->validate($model->rules)) {
            $data['validation'] = $this->validator;
        } else {
            $file = $this->request->getFile('lien');

            $insert = [
                'titre' => $this->request->getVar('titre'),
            ];
            if($file->isValid() && !$file->hasMoved()){
                $file->move($this->relativepath);
                $insert['lien'] = $this->path.$file->getName();
            }

            $id = $model->insert($insert);
            $session = session();
            $session->setFlashData('success', 'Document ajouté');
            return redirect()->to("/documents/detail/".$id);
        }
        return view('documents/index',$data);
    }

    public function edit($id = NULL){
        $data = [];
        helper(['form']);
        $model = new DocumentModel();
        $data['documents'] = $model->find($id);
        if($data['documents']){
            return view('documents/edit',$data);
        }
        else{
            $session = session();
			$session->setFlashData('error', "Document non trouvé");
            return redirect()->to("/documents/index");
        }
    }

    public function update($id = null)
    {
        $data = [];
        helper(['form']);
        $model = new DocumentModel();
        $data['documents'] = $model->find($id);
        if($data['documents']){
            if (!$this->validate($model->rules)) {
                $data['validation'] = $this->validator;
                $data['documents']['id'] = $id;
            } else {
                $document = $data['document']['lien'];
                $file = $this->request->getFile('lien');

                $update = [
                    'id' => $id,
                    'titre' => $this->request->getVar('titre'),
                ];
                
                if($file->isValid() && !$file->hasMoved()){
                    $file->move($this->relativepath);
                    if(file_exists("./".$document)){
                        unlink("./".$document);
                    }
                    $update['lien'] = $this->path.$file->getName();;
                }

                $model->save($update);
                $session = session();
                $session->setFlashData('success', 'Document modifié');
                return redirect()->to("/documents/detail/".$id);
            }
            return view('documents/edit',$data);
        }else{
            $session = session();
            $session->setFlashData('error', 'Document non trouvé');
            return redirect()->to("/documents/index");
        }
    }

    public function delete($id = null)
    {
        $model = new DocumentModel();
        $data = $model->find($id);
        if ($data) {
            $document = $data['lien'];
            $titre = $data['titre'];
            $model->delete($id);
            if(file_exists("./".$document)){
                unlink("./".$document);
            }
            $session = session();
			$session->setFlashData('success', "Document : '".$titre."' a été supprimé");
            return redirect()->to("/documents/index");
        } else {
            $session = session();
			$session->setFlashData('error', "Document non trouvé");
            return redirect()->to("/documents/index");
        }
    }
}
