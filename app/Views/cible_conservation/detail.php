<?= $this->extend('templates/admin_template') ?>

<?= $this->section('content') ?>

<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Cible conservation détail</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= site_url('/cible_conservation') ?>">Accueil</a></li>
              <li class="breadcrumb-item active">Détail</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
        <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-sm-10">
            <h3 class="card-title">
                <i class="fas fa-newspaper"></i>
                <?= $cible[0]->nom_cible_conservation ?>
            </h3>
            </div>
            <div class="col-sm-2">
            <a href="<?= site_url('cible_conservation/edit/') ?><?= $cible[0]->id?>"><button type="button" class="btn btn-block bg-gradient-success btn-sm">Modifier</button></a>
            </div>
          </div>
          </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="row">
                <div class="col-sm-3"></div>
                <div class="col-sm-6">
                  <?php if($cible[0]->image_cible != null):?>
                    <img class="img-fluid" src="<?= $cible[0]->image_cible ?>" alt="Photo">
                  <?php endif;?>
                </div>
                <div class="col-sm-3"></div>
            </div>
            <blockquote>
                <b>Type</b>
                <p><?= $cible[0]->type_cible_conservation?></p>
                <b>Description</b>
                <p><?= $cible[0]->description_cible_conservation ?></p>
                <b>Localisation</b>
                <p><?= $cible[0]->nom_du_site?></p>
            </blockquote>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
      </div><!-- /.container-fluid -->
    </div>

<?= $this->endSection('') ?>