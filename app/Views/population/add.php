<?= $this->extend('templates/admin_template') ?>

<?= $this->section('content') ?>

<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Population locale</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?= site_url('/population') ?>">Accueil</a></li>
                    <li class="breadcrumb-item active">Ajout</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">Population locale</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                </div>
            </div>
            <?php if (isset($validation)) : ?>
                <div class="row">
                    <div class="col-12">
                        <div class="card card-danger direct-chat direct-chat-danger shadow-lg">
                            <div class="card-header">
                                <h3 class="card-title">Erreur(s)</h3>
                            </div>
                            <div class="card-body">
                                <?= $validation->listErrors() ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <!-- /.card-header -->
            <div class="card-body">
                <!-- /.col (left) -->
                <form action="<?= site_url('population/add') ?>" method="post" enctype='multipart/form-data'>

                    <div class="col-md-12">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Informations générales</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Nom de la population</label>
                                            <input type="text" class="form-control" name="nom_population" placeholder="Nom de la population" value="<?= set_value('nom_population') ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Aire protégée auquel appartient le cible</label>
                                            <select class="form-control select2bs4" name="id_aire_protegee" style="width: 100%;">
                                                <?php foreach ($ap as $item) : ?>
                                                    <option value="<?= $item['id_ap'] ?>" <?= set_select('id_aire_protegee', $item['id_ap']); ?>><?= $item['nom_du_site'] ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="card card-outline card-info">
                                            <div class="card-header">
                                                <h3 class="card-title">
                                                    Description
                                                </h3>
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body pad">
                                                <div class="mb-3">
                                                    <textarea class="textarea" placeholder="Place some text here" name="description" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?= set_value('description') ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="card card-outline card-info">
                                            <div class="card-header">
                                                <h3 class="card-title">
                                                    Impacts
                                                </h3>
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body pad">
                                                <div class="mb-3">
                                                    <textarea class="textarea" placeholder="Place some text here" name="impacts" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?= set_value('impacts') ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card -->
                        <div class="form-group">
                            <label>Image</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="exampleInputFile" name="image_population">
                                <label class="custom-file-label" for="exampleInputFile">Importer une image</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-block btn-success btn-lg">Ajouter la population</button>
                    </div>
                </form>
            </div>
            <!-- /.card -->

            <!-- /.row -->
        </div><!-- /.container-fluid -->
</section>
<!-- /.content -->


<?= $this->endSection('') ?>