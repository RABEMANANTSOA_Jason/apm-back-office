<?php

namespace App\Models;

use CodeIgniter\Model;

class Statut_Categorie_iucnModel extends Model
{
    protected $table = 'Statut_Categorie_IUCN';
    protected $primaryKey = 'id';
    protected $allowedFields = ['code_statut_categorie_iucn', 'statut_fr', 'categorie', 'statut_mg','description_categorie_iucn'];
    protected $rules = [
        'code_statut_categorie_iucn' => [
            'rules' => 'required|is_unique[Statut_Categorie_IUCN.code_statut_categorie_iucn]',
            'errors' => [
                'required' => "Veuillez donner une abbréviation",
                'is_unique' => "Cette abréviation existe déjà"
            ]
        ],
        'statut_fr' => [
            'rules' => 'required|is_unique[statut_categorie_iucn.statut_fr]',
            'errors' => [
                'required' => "Veuillez donner un statut en français",
                'is_unique' => "Ce statut existe déjà"
            ]
        ],
        'categorie' => [
            'rules' => 'required|is_unique[statut_categorie_iucn.categorie]',
            'errors' => [
                'required' => "Veuillez donner une catégorie",
                'is_unique' => "Cette catégorie existe déjà"
            ]
        ]
    ];

    protected $update_rules = [
        'code_statut_categorie_iucn' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez donner une abbréviation",
            ]
        ],
        'statut_fr' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez donner un statut en français",
            ]
        ],
        'categorie' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez donner une catégorie",
            ]
        ]
    ];
}
