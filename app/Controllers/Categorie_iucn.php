<?php namespace App\Controllers;

use App\Models\Statut_Categorie_iucnModel;

class Categorie_iucn extends BaseController
{
    private $notFound = 'Catégorie IUCN non trouvé';

    public function index()
    {
        $data = [];
        $model = new Statut_Categorie_iucnModel();
        $data['categorie_iucn'] = $model->findAll();
        return view('categorie_iucn/index',$data);
    }

    public function detail($id = NULL){
        $data = [];
        $model = new Statut_Categorie_iucnModel();
        $data['categorie_iucn'] = $model->find($id);
        if($data['categorie_iucn']){
            return view('categorie_iucn/detail',$data);
        }
        else{
            $session = session();
			$session->setFlashData('error', $this->notFound);
            return redirect()->to("/categorie_iucn/index");
        }
    }

    public function create(){
        helper(['form']);
        return view('categorie_iucn/add');
    }

    public function add()
    {
        $data = [];
        helper(['form']);
        $model = new Statut_Categorie_iucnModel();        
        if (!$this->validate($model->rules)) {
            $data['validation'] = $this->validator;
        } else {
            $insert = [
                'code_statut_categorie_iucn' => $this->request->getVar('code_statut_categorie_iucn'),
                'statut_fr' => $this->request->getVar('statut_fr'),
                'categorie' => $this->request->getVar('categorie'),
                'statut_mg' => $this->request->getVar('statut_mg'),
                'description_categorie_iucn' => $this->request->getVar('description_categorie_iucn'),
            ];

            $id = $model->insert($insert);
            $session = session();
            $session->setFlashData('success', 'Catégorie IUCN ajoutée');
            return redirect()->to("/categorie_iucn/detail/".$id);
        }
        return view('categorie_iucn/add',$data);
    }

    public function edit($id = NULL){
        $data = [];
        helper(['form']);
        $model = new Statut_Categorie_iucnModel();
        $data['categorie_iucn'] = $model->find($id);
        if($data['categorie_iucn']){
            return view('categorie_iucn/edit',$data);
        }
        else{
            $session = session();
			$session->setFlashData('error', $this->notFound);
            return redirect()->to("/categorie_iucn/index");
        }
    }

    public function update($id = null)
    {
        $data = [];
        helper(['form']);
        $model = new Statut_Categorie_iucnModel();
        $data['categorie_iucn'] = $model->find($id);
        if($data['categorie_iucn']){
            if (!$this->validate($model->update_rules)) {
                $data['validation'] = $this->validator;
                $data['categorie_iucn']['id'] = $id;
            } else {

                $update = [
                    'id' => $id,
                    'code_statut_categorie_iucn' => $this->request->getVar('code_statut_categorie_iucn'),
                    'statut_fr' => $this->request->getVar('statut_fr'),
                    'categorie' => $this->request->getVar('categorie'),
                    'statut_mg' => $this->request->getVar('statut_mg'),
                    'description_categorie_iucn' => $this->request->getVar('description_categorie_iucn'),
                ];

                $model->save($update);
                $session = session();
                $session->setFlashData('success', 'Catégorie IUCN modifiée');
                return redirect()->to("/categorie_iucn/detail/".$id);
            }
            return view('categorie_iucn/edit',$data);
        }else{
            $session = session();
            $session->setFlashData('error', $this->notFound);
            return redirect()->to("/categorie_iucn/index");
        }
    }

    public function delete($id = null)
    {
        $model = new Statut_Categorie_iucnModel();
        $data = $model->find($id);
        if ($data) {
            $statut_fr = $data['statut_fr'];
            $model->delete($id);
            $session = session();
			$session->setFlashData('success', "Catégorie IUCN : '".$statut_fr."' a été supprimée");
            return redirect()->to("/categorie_iucn/index");
        } else {
            $session = session();
			$session->setFlashData('error', $this->notFound);
            return redirect()->to("/categorie_iucn/index");
        }
    }
}
