<?php

namespace App\Models;

use CodeIgniter\Model;

class UtilisateurModel extends Model
{
    protected $table = 'Utilisateurs';
    protected $primaryKey = 'id';
    protected $allowedFields = ['nom', 'prenom', 'utilisateur', 'password', 'date_creation'];

    protected $rules = [
        'nom' => [
            'rules' => 'required|min_length[3]|max_length[50]',
            'errors' => [
                'required' => "Veuillez donner un nom pour l'utilisateur",
                'min_length' => "Le nom d'utilisateur doit faire 3 caractères au moins",
                'max_length' => "Le nom d'utilisateur ne doit pas excéder les 50 caractères"
            ]
        ],
        'prenom' => [
            'rules' => 'required|min_length[3]|max_length[50]',
            'errors' => [
                'required' => "Veuillez donner un prénom pour l'utilisateur",
                'min_length' => "Le prénom de l'utilisateur doit faire 3 caractères au moins",
                'max_length' => "Le prénom de l'utilisateur ne doit pas excéder les 50 caractères"
            ]
        ],
        'utilisateur' => [
            'rules' => 'required|min_length[4]|max_length[50]|is_unique[utilisateurs.utilisateur]',
            'errors' => [
                'required' => "Veuillez fournir un utilisateur",
                'min_length' => "L'utilisateur doit faire 4 caractères au moins",
                'max_length' => "L'utilisateur ne doit pas excéder les 50 caractères",
                'is_unique' => 'Cet utilisateur existe déjà'
            ]
        ],
        'password' => [
            'rules' => 'required|min_length[8]',
            'errors' => [
                'required' => "Veuillez fournir un mot de passe",
                'min_length' => "Le mot de passe doit faire 8 caractères alpha-numérique au moins",
            ]
        ],
        'password_confirm' => [
            'rules' => 'required|matches[password]',
            'errors' => [
                'required' => "Veuillez confirmer votre mot de passe",
                'matches' => "Mot de passe différent"
            ]
        ],

    ];

    protected $beforeInsert = ['beforeInsert'];
    protected $beforeUpdate = ['beforeUpdate'];

    protected function beforeInsert(array $data)
    {
        $data = $this->passwordHash($data);
        return $data;
    }

    protected function beforeUpdate(array $data)
    {
        $data = $this->passwordHash($data);
        return $data;
    }

    protected function passwordHash(array $data)
    {
        if (isset($data['data']['password'])) {
            $data['data']['password'] = password_hash($data['data']['password'], PASSWORD_DEFAULT);
        }
        return $data;
    }
}
