<?php namespace App\Controllers;

use App\Models\VisiteVirtuelleModel;
use App\Models\custom\VisiteVirtuelleCustomModel;
use App\Models\AireProtegeModel;

class Visite_virtuelle extends BaseController
{
    private $path = '/images/visites/';
    private $relativepath = './images/visites';

    public function index()
    {
        $data = [];
        $model = new VisiteVirtuelleModel();
        $data['visites'] = $model->findAll();
        return view('visite_virtuelle/index',$data);
    }

    public function create()
    {
        $data = [];
        helper(['form']);
        $model = new AireProtegeModel();
        $data['liste_ap'] = $model->findAll();
        return view('visite_virtuelle/add',$data);
    }

    public function add(){
        $data = [];
        helper(['form']);

        $model = new VisiteVirtuelleModel();
        $ap = new AireProtegeModel();
        if(!$this->validate($model->rules)){
            $data['liste_ap'] = $ap->findAll();
            $data['validation'] = $this->validator;
            return view('visite_virtuelle/add',$data);
        }
        else{
            $fileNord = $this->request->getFile('image_nord');
            $fileOuest = $this->request->getFile('image_ouest');
            $fileCentre = $this->request->getFile('image_centre');
            $fileEst = $this->request->getFile('image_est');
            $fileSud = $this->request->getFile('image_sud');
            if($fileCentre->isValid() && !$fileCentre->hasMoved()){
                $insert = [
                    'nom_visite' => $this->request->getVar('nom_visite'),
                    'id_aire_protegee' => $this->request->getVar('id_aire_protegee'),
                    'image_centre' => $this->path.$fileCentre->getName(),
                ];
                if($fileNord->isValid() && !$fileNord->hasMoved()){$fileNord->move($this->relativepath);$insert['image_nord'] = $this->path.$fileNord->getName();}
                if($fileOuest->isValid() && !$fileOuest->hasMoved()){$fileOuest->move($this->relativepath);$insert['image_ouest'] = $this->path.$fileOuest->getName();}
                if($fileEst->isValid() && !$fileEst->hasMoved()){$fileEst->move($this->relativepath);$insert['image_est'] = $this->path.$fileEst->getName();}
                if($fileSud->isValid() && !$fileSud->hasMoved()){$fileSud->move($this->relativepath);$insert['image_sud'] = $this->path.$fileSud->getName();}

                $id = $model->save($insert);
                $fileCentre->move($this->relativepath);
                $session = session();
                $session->setFlashData('success', 'Visite virtuelle enregistrée : ' . $insert['nom_visite']);
                return redirect()->to("/visite_virtuelle/detail/".$id);
            }
            else{
                $session = session();
                $session->setFlashData('error', "Erreur");
            }
        }
        return view('visite_virtuelle/add',$data);
    }

    public function detail($id = NULL)
    {
        $data = [];
        $db = db_connect();
        $model = new VisiteVirtuelleCustomModel($db);
        $data['visite'] = $model->getDetailVisiste($id);
        if($data['visite']){
            return view('visite_virtuelle/detail',$data);
        }
        else{
            $session = session();
			$session->setFlashData('error', "Visite virtuelle non trouvée");
            return redirect()->to("/visite_virtuelle/index");
        }
    }

    
    public function edit($id = NULL){
        $data = [];
        helper(['form']);
        $db = db_connect();

        $model = new VisiteVirtuelleModel();
        $ap = new AireProtegeModel();
        $data['liste_ap'] = $ap->findAll();
        $data['visite'] = $model->find($id);
        if($data['visite']){
            return view('visite_virtuelle/edit',$data);
        }
        else{
            $session = session();
			$session->setFlashData('error', "Visite non trouvée");
            return redirect()->to("/visite_virtuelle/index");
        }
    }

    public function update($id = null)
    {
        $data = [];
        helper(['form']);
        $db = db_connect();

        $model = new VisiteVirtuelleModel();
        $modelCustom = new VisiteVirtuelleCustomModel($db);
        $ap = new AireProtegeModel();
        $data['visite'] = $model->find($id);
        if($data['visite']){
            if(!$this->validate($model->rules_update)){
                $data['liste_ap'] = $ap->findAll();
                $data['validation'] = $this->validator;
                $data['id'] = $id;
                return view('visite_virtuelle/edit',$data);
            } else {
                $image_nord = $data['visite']['image_nord'];
                $image_ouest = $data['visite']['image_ouest'];
                $image_centre = $data['visite']['image_centre'];
                $image_est = $data['visite']['image_est'];
                $image_sud = $data['visite']['image_sud'];

                $fileNord = $this->request->getFile('image_nord');
                $fileOuest = $this->request->getFile('image_ouest');
                $fileCentre = $this->request->getFile('image_centre');
                $fileEst = $this->request->getFile('image_est');
                $fileSud = $this->request->getFile('image_sud');
                
                $update = [
                    'id' => $id,
                    'nom_visite' => $this->request->getVar('nom_visite'),
                    'id_aire_protegee' => $this->request->getVar('id_aire_protegee'),
                ];
                if($fileNord->isValid() && !$fileNord->hasMoved()){$fileNord->move($this->relativepath);if(file_exists("./".$image_nord) && !is_dir("./".$image_nord)){unlink("./".$image_nord);}$update['image_nord'] = $this->path.$fileNord->getName();}
                if($fileOuest->isValid() && !$fileOuest->hasMoved()){$fileOuest->move($this->relativepath);if(file_exists("./".$image_ouest) && !is_dir("./".$image_ouest)){unlink("./".$image_ouest);}$update['image_ouest'] = $this->path.$fileOuest->getName();}
                if($fileCentre->isValid() && !$fileCentre->hasMoved()){$fileCentre->move($this->relativepath);if(file_exists("./".$image_centre) && !is_dir("./".$image_centre)){unlink("./".$image_centre);}$update['image_centre'] = $this->path.$fileCentre->getName();}
                if($fileEst->isValid() && !$fileEst->hasMoved()){$fileEst->move($this->relativepath);if(file_exists("./".$image_est) && !is_dir("./".$image_est)){unlink("./".$image_est);}$update['image_est'] = $this->path.$fileEst->getName();}
                if($fileSud->isValid() && !$fileSud->hasMoved()){$fileSud->move($this->relativepath);if(file_exists("./".$image_sud) && !is_dir("./".$image_sud)){unlink("./".$image_sud);}$update['image_sud'] = $this->path.$fileSud->getName();}
    
                $model->save($update);
                $session = session();
                $session->setFlashData('success', 'Visite virtuelle modifiée : ' . $update['nom_visite']);
                return redirect()->to("/visite_virtuelle/detail/".$id);
            }
            return view('visite_virtuelle/edit',$data);
        }else{
            $session = session();
            $session->setFlashData('error', 'Visite non trouvée');
            return redirect()->to("/visite_virtuelle/index");
        }
    }

    /*public function delete($id = null)
    {
        $model = new VisiteVirtuelleModel();
        $data = $model->find($id);
        if ($data) {
            $nom_visite = $data['nom_visite'];
            $image = $data['image_actualite'];
            $model->delete($id);
            if(file_exists("./".$image)){
                unlink("./".$image);
            }
            $session = session();
			$session->setFlashData('success', "Visite : '".$titre."' a été supprimée");
            return redirect()->to("/actualites/index");
        } else {
            $session = session();
			$session->setFlashData('error', "Actualité non trouvée");
            return redirect()->to("/actualites/index");
        }
    }*/

    
}
