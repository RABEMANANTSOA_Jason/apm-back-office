<?php namespace App\Controllers;

use App\Models\AireProtegeModel;
use App\Models\custom\AireProtegeeCustomModel;
use App\Models\Statut_Categorie_iucnModel;
use App\Models\RegionModel;
use App\Models\DistrictModel;
use App\Models\CommuneModel;
use App\Models\FokontanyModel;
use App\Models\GestionnaireModel;
use App\Models\Mode_GestionModel;
use App\Models\GeolocalisationModel;
use App\Models\custom\CibleConservationCustomModel;

class Aire_protegee extends BaseController
{
    private $path = '/images/ap/';
    private $relativepath = './images/ap';

    public function index()
    {
        $data = [];
        $db = db_connect();
        $model = new AireProtegeeCustomModel($db);
        $data['listeAP'] = $model->getAPindex();
        /*echo '<pre>';
            print_r($data['listeAP']);
        echo '<pre>';*/
        return view('aire_protegee/index',$data);
    }



    public function create(){
        $data = [];
        helper(['form']);
        $statuts = new Statut_Categorie_iucnModel();
        $gestionnaires = new GestionnaireModel();
        $mode_gestion = new Mode_GestionModel();
        $region = new RegionModel();
        $district = new DistrictModel();
        $commune = new CommuneModel();
        $fokontany = new FokontanyModel();
        $data['statuts'] = $statuts->findAll();
        $data['gestionnaires'] = $gestionnaires->findAll();
        $data['mode_gestion'] = $mode_gestion->findAll();
        $data['regions'] = $region->findAll();
        $data['districts'] = $district->findAll();
        $data['communes'] = $commune->findAll();
        $data['fokontany'] = $fokontany->findAll();
        return view('aire_protegee/add',$data);
    }

    public function add(){
        $data = [];
        helper(['form']);
        $db = db_connect();
        $statuts = new Statut_Categorie_iucnModel();
            $gestionnaires = new GestionnaireModel();
            $mode_gestion = new Mode_GestionModel();
            $region = new RegionModel();
            $district = new DistrictModel();
            $commune = new CommuneModel();
            $fokontany = new FokontanyModel();
            $data['statuts'] = $statuts->findAll();
            $data['gestionnaires'] = $gestionnaires->findAll();
            $data['mode_gestion'] = $mode_gestion->findAll();
            $data['regions'] = $region->findAll();
            $data['districts'] = $district->findAll();
            $data['communes'] = $commune->findAll();
            $data['fokontany'] = $fokontany->findAll();
        $model = new AireProtegeModel();
        if(!$this->validate($model->rules)){
            $data['validation'] = $this->validator;
            return view('aire_protegee/add',$data);
        }
        else{
            try{
            $file = $this->request->getFile('ap_image');
            $insertAP = [
                'nom_du_site' => $this->request->getVar('nom_du_site'),
                'date_creation_ap' => $this->request->getVar('date_creation_ap'),
                'date_signature' => $this->request->getVar('date_signature'),
                'arrete_mise_en_protection' => $this->request->getVar('arrete_mise_en_protection'),
                'date_mise_en_protection' => $this->request->getVar('date_mise_en_protection'),
                'arrete_decret_en_cours' => $this->request->getVar('arrete_decret_en_cours'),
                'numero_decret' => $this->request->getVar('numero_decret'),
                'type_ap' => $this->request->getVar('type_ap'),
                'id_statut_categorie_iucn' => $this->request->getVar('id_statut_categorie_iucn'),
                'id_gestionnaire' => $this->request->getVar('id_gestionnaire'),
                'id_mode_gestion' => $this->request->getVar('id_mode_gestion'),
                'historique' => $this->request->getVar('historique'),
                'regions' => $this->request->getVar('regions'),
                'districts' => $this->request->getVar('districts'),
                'communes' => $this->request->getVar('communes'),
                'fokontany' => $this->request->getVar('fokontany'),
                'superficie_decret' => $this->request->getVar('superficie_decret'),
                'superficie_nd' => $this->request->getVar('superficie_nd'),
                'superficie_zt' => $this->request->getVar('superficie_zt'),
                'potentialites' => $this->request->getVar('potentialites'),
                'menaces' => $this->request->getVar('menaces'),
            ];
            $insertGeo = [
                'geojson' => $this->request->getVar('geojson'),
            ]; 
            if($file->isValid() && !$file->hasMoved()){
                $file->move($this->relativepath);
                $insertAP['ap_image'] = $this->path.$file->getName();
            }
            $customModel = new AireProtegeeCustomModel($db);
            $id = $customModel->insertAireProtegee($insertAP,$insertGeo);
            $session = session();
            $session->setFlashData('success', 'Aire protégée enregistrée : ' . $insertAP['nom_du_site']);
            return redirect()->to("/aire_protegee/detail/".$id);
            }catch(\Exception $e){
                $session = session();
                $session->setFlashData('error', $e->getMessage());
                return view('aire_protegee/add',$data);
            }

        }
        /*echo '<pre>';
            print_r(implode(";",$this->request->getVar('region')));
        echo '<pre>';*/
        return view('ap/add',$data);
    }

    public function detail($id = NULL){
        $data = [];
        $db = db_connect();
        $model = new AireProtegeeCustomModel($db);
        $data['aire_protegee'] = $model->getAPDetail($id);
        if($data['aire_protegee']){
            /*echo '<pre>';
            print_r($data['aire_protegee']);
            echo '<pre>';*/
            $cible_conservation = new CibleConservationCustomModel($db);
            $data['cible_conservation'] = $cible_conservation->getCibleConservationAP($id);
            return view('aire_protegee/detail',$data);
        }
        else{
            $session = session();
			$session->setFlashData('error', "Aire protégée non trouvée");
            return redirect()->to("/aire_protegee/index");
        }
    }

    public function edit($id = NULL){
        $data = [];
        helper(['form']);
        $db = db_connect();
        $model = new AireProtegeModel();
        $customModel = new AireProtegeeCustomModel($db);
        $data['ap'] = $model->find($id);


        if($data['ap']){
            $statuts = new Statut_Categorie_iucnModel();
            $gestionnaires = new GestionnaireModel();
            $mode_gestion = new Mode_GestionModel();
            $region = new RegionModel();
            $district = new DistrictModel();
            $commune = new CommuneModel();
            $fokontany = new FokontanyModel();
            $data['statuts'] = $statuts->findAll();
            $data['gestionnaires'] = $gestionnaires->findAll();
            $data['mode_gestion'] = $mode_gestion->findAll();
            $data['regions'] = $region->findAll();
            $data['districts'] = $district->findAll();
            $data['communes'] = $commune->findAll();
            $data['fokontany'] = $fokontany->findAll();
            $data['geolocalisation'] = $customModel->getGeolocalisationDetai($data['ap']['id_ap']);
            $data['ap']['regions'] = explode(";",$data['ap']['regions']);
            $data['ap']['districts'] = explode(";",$data['ap']['districts']);
            $data['ap']['communes'] = explode(";",$data['ap']['communes']);
            $data['ap']['fokontany'] = explode(";",$data['ap']['fokontany']);

            return view('aire_protegee/edit',$data);
        }
        else{
            $session = session();
			$session->setFlashData('error', "Aire protégée non trouvée");
            return redirect()->to("/aire_protegee/index");
        }
    }

    public function update($id = NULL){
        $data = [];
        helper(['form']);
        $model = new AireProtegeModel();
        $db = db_connect();
        $customModel = new AireProtegeeCustomModel($db);
        $data['ap'] = $model->find($id);
        $statuts = new Statut_Categorie_iucnModel();
        $gestionnaires = new GestionnaireModel();
        $mode_gestion = new Mode_GestionModel();
        $region = new RegionModel();
        $district = new DistrictModel();
        $commune = new CommuneModel();
        $fokontany = new FokontanyModel();
        $data['statuts'] = $statuts->findAll();
        $data['gestionnaires'] = $gestionnaires->findAll();
        $data['mode_gestion'] = $mode_gestion->findAll();
        $data['regions'] = $region->findAll();
        $data['districts'] = $district->findAll();
        $data['communes'] = $commune->findAll();
        $data['fokontany'] = $fokontany->findAll();
        $data['ap']['id'] = $id;
        $data['ap']['regions'] = explode(";",$data['ap']['regions']);
        $data['ap']['districts'] = explode(";",$data['ap']['districts']);
        $data['ap']['communes'] = explode(";",$data['ap']['communes']);
        $data['ap']['fokontany'] = explode(";",$data['ap']['fokontany']);
        $data['geolocalisation'] = $customModel->getGeolocalisationDetai($data['ap']['id_ap']);
        if($data['ap']){
            if (!$this->validate($model->rules)) {
                $data['validation'] = $this->validator;
            } else {
                $image = $data['ap']['ap_image'];
                $file = $this->request->getFile('ap_image');

                $updateAP = [
                    'nom_du_site' => $this->request->getVar('nom_du_site'),
                    'date_creation_ap' => $this->request->getVar('date_creation_ap'),
                    'date_signature' => $this->request->getVar('date_signature'),
                    'arrete_mise_en_protection' => $this->request->getVar('arrete_mise_en_protection'),
                    'date_mise_en_protection' => $this->request->getVar('date_mise_en_protection'),
                    'arrete_decret_en_cours' => $this->request->getVar('arrete_decret_en_cours'),
                    'numero_decret' => $this->request->getVar('numero_decret'),
                    'type_ap' => $this->request->getVar('type_ap'),
                    'id_statut_categorie_iucn' => $this->request->getVar('id_statut_categorie_iucn'),
                    'id_gestionnaire' => $this->request->getVar('id_gestionnaire'),
                    'id_mode_gestion' => $this->request->getVar('id_mode_gestion'),
                    'historique' => $this->request->getVar('historique'),
                    'regions' => $this->request->getVar('regions'),
                    'districts' => $this->request->getVar('districts'),
                    'communes' => $this->request->getVar('communes'),
                    'fokontany' => $this->request->getVar('fokontany'),
                    'superficie_decret' => $this->request->getVar('superficie_decret'),
                    'superficie_nd' => $this->request->getVar('superficie_nd'),
                    'superficie_zt' => $this->request->getVar('superficie_zt'),
                    'potentialites' => $this->request->getVar('potentialites'),
                    'menaces' => $this->request->getVar('menaces'),
                    'ap_image' => $image,
                ];
                $updateGeo = [
                    'geojson' => $this->request->getVar('geojson'),
                ]; 
                if($file->isValid() && !$file->hasMoved()){
                    $file->move($this->relativepath);
                    if(file_exists("./".$image) && !is_dir("./".$image)){
                        unlink("./".$image);
                    }
                    $updateAP['ap_image'] = $this->path.$file->getName();;
                }

                $customModel->updateAireProtegee($updateAP,$id,$updateGeo,$this->request->getVar('id_geolocalisation'));
                $session = session();
                $session->setFlashData('success', 'Aire protégée modifiée');
                return redirect()->to("/aire_protegee/detail/".$id);
            }
            return view('ap/edit',$data);
        }else{
            $session = session();
            $session->setFlashData('error', "Aire protégée non trouvée");
            return redirect()->to("/aire_protegee/index");
        }
    }

    public function delete($id = null)
    {
        $model = new AireProtegeModel();
        $data = $model->find($id);
        if ($data) {
            $nom = $data['nom_cible_conservation'];
            $image = $data['image_cible'];
            $model->delete($id);
            if(file_exists("./".$image) && !is_dir("./".$image)){
                unlink("./".$image);
            }
            $session = session();
			$session->setFlashData('success', "Cible de conservation : '".$nom."' a été supprimée");
            return redirect()->to("/aire_protegee/index");
        } else {
            $session = session();
			$session->setFlashData('error', "Cible de conservation non trouvée");
            return redirect()->to("/aire_protegee/index");
        }
    }
    //--------------------------------------------------------------------
}
