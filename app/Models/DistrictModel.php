<?php

namespace App\Models;

use CodeIgniter\Model;

class DistrictModel extends Model
{
    protected $table = 'Districts';
    protected $primaryKey = 'id';
    protected $allowedFields = ['district', 'id_region'];
    protected $rules = [
        'district' => [
            'rules' => 'required|is_unique[districts.district]',
            'errors' => [
                'required' => "Veuillez donner un nom de district",
                'is_unique' => "Cette district existe déjà"
            ]
        ],
        'id_region' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez référence une région",
            ]
        ]
    ];

    public function getDistrictAll(){
        return $this->findAll();
    }
}
