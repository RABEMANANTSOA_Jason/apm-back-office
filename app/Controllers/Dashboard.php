<?php namespace App\Controllers;

use App\Models\AireProtegeModel;

class Dashboard extends BaseController
{
	public function index()
	{
		$data = [];
		$model = new AireProtegeModel();
		$data['nombreAP'] = $model->getNombreAireProtegee();
		return view('dashboard/index',$data);
	}

	//--------------------------------------------------------------------

}
