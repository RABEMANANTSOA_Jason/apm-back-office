<?= $this->extend('templates/admin_template') ?>

<?= $this->section('content') ?>

<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Mode de gestion</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= ('mode_gstion') ?>">Accueil</a></li>
              <li class="breadcrumb-item active">Mode de gestion</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-sm-10">
                    <h3 class="card-title">Les modes de gestion</h3>
                  </div>
                  <div class="col-sm-2">
                  <a href="<?= site_url('mode_gestion/create') ?>"><button type="button" class="btn btn-block bg-gradient-success btn-sm">Ajouter un mode de gestion</button></a>
                  </div><!-- /.col -->
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Mode</th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php foreach ($mode_gestion as $item):?>
                    <tr>
                      <td><?= $item['type_mode_gestion'] ?></td>
                      <td><a href="<?= site_url('mode_gestion/detail/') ?><?= $item['id']?>">Voir</a> | <a href="<?= site_url('mode_gestion/edit/') ?><?= $item['id']?>"">Modifier</a></td>
                    </tr>
                  <?php endforeach;?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

<?= $this->endSection('') ?>