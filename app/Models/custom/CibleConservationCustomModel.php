<?php

namespace App\Models\custom;

use CodeIgniter\Database\ConnectionInterface;

class CibleConservationCustomModel
{
    protected $db;

    public function __construct(ConnectionInterface &$db)
    {
        $this->db =& $db;
    }

    public function getCibleConservationAP($id){
        $builder = $this->db->table('Cible_Conservation');
        $builder->where(['Cible_Conservation.id_aire_protegee' => $id]);
        $result = $builder->get()->getResult();
        return $result;
    }

    public function getDetailCible($id){
        $builder = $this->db->table('Cible_Conservation');
        $builder->where(['Cible_Conservation.id' => $id]);
        $builder->join('Aires_Protegees','Aires_Protegees.id_ap = Cible_Conservation.id_aire_protegee');
        $result = $builder->get()->getResult();
        return $result;
    }
}
