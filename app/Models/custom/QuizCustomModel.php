<?php

namespace App\Models\custom;

use CodeIgniter\Database\ConnectionInterface;

class QuizCustomModel
{
    protected $db;

    public function __construct(ConnectionInterface &$db)
    {
        $this->db = &$db;
    }

    public function getDetailQuiz($id)
    {
        $builder = $this->db->table('Quiz');
        $builder->where(['Quiz.id_quiz' => $id]);
        $builder->join('Quiz_detail', 'Quiz_detail.id_quiz = Quiz.id_quiz');
        $result = $builder->get()->getResult();
        return $result;
    }

    public function beforeInsert(array $data)
    {
        if ($data['reponse'] == 1) {
            $data['reponse'] = $data['optionA'];
        }
        if ($data['reponse'] == 2) {
            $data['reponse'] = $data['optionB'];
        }
        if ($data['reponse'] == 3) {
            $data['reponse'] = $data['optionC'];
        }
        if ($data['reponse'] == 4) {
            $data['reponse'] = $data['optionD'];
        }
        return $data;
    }

    public function insertQuiz($quiz, $detail)
    {
        $this->db->transStart();
        $builder = $this->db->table('Quiz');
        $builder->insert($quiz);
        $id = $this->db->insertID();

        $detail['id_quiz'] = $id;
        $builder = $this->db->table('Quiz_detail');
        $builder->insert($this->beforeInsert($detail));
        $this->db->transComplete();
        return $id;
    }

    public function insertQestion($detail)
    {
        $builder = $this->db->table('Quiz_detail');
        $builder->insert($this->beforeInsert($detail));
    }
}
