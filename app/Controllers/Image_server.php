<?php namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;

class Image_server extends ResourceController
{

    public function getimage()
    {
        $file = '.'.$this->request->getVar('path');

        if (file_exists($file)) {
            header('Content-Description: image');
            header('Content-Type: image/jpeg');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
            exit;
        }
    }
}