<?php namespace App\Controllers;

use App\Models\FeedBackModel;
class Feedback extends BaseController
{
    public function index()
    {
        $data = [];
        $model = new FeedBackModel();
        $data['feedback'] = $model->findAll();
        return view('feedback/index',$data);
    }

    public function detail($id = NULL)
    {
        $data = [];
        $model = new FeedBackModel();
        $data['feedback'] = $model->find($id);
        if($data['feedback']){
            return view('feedback/detail',$data);
        }
        else{
            $session = session();
			$session->setFlashData('error', "Message non trouvé");
            return redirect()->to("/feedback/index");
        }
        return view('feedback/detail');
    }

}
