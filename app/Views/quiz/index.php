<?= $this->extend('templates/admin_template') ?>

<?= $this->section('content') ?>

<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Quiz</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= ('dashboard') ?>">Accueil</a></li>
              <li class="breadcrumb-item active">Quiz</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-sm-10">
                    <h3 class="card-title">Liste des quiz</h3>
                  </div>
                  <div class="col-sm-2">
                  <a href="<?= site_url('quiz/create') ?>"><button type="button" class="btn btn-block bg-gradient-success btn-sm">Nouveau quiz</button></a>
                  </div><!-- /.col -->
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              <?php $i = 0;?>
              <?php foreach ($quiz as $item):?>
                <?php if($i === 0):?>
                  <div class="row mt-4">
                <?php endif;?>
                  <div class="col-sm-4">
                    <div class="callout callout-info">
                      <h5><?= $item['nom_quiz']?></h5>
                      <a href="<?= site_url('quiz/detail/')?><?= $item['id_quiz']?>">Consulter</a>
                    </div>
                  </div>
                <?php $i++;?>
                  <?php if($i === 3): ?>
                    <?php $i = 0;?> 
                    </div> 
                  <?php endif; ?>
              <?php endforeach; ?>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

<?= $this->endSection('') ?>