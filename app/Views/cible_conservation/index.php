<?= $this->extend('templates/admin_template') ?>

<?= $this->section('content') ?>

<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Cibles de conservations</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= ('dashboard') ?>">Accueil</a></li>
              <li class="breadcrumb-item active">Cibles de conservations</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-sm-10">
                    <h3 class="card-title">Cibles de conservations répertoriés</h3>
                  </div>
                  <div class="col-sm-2">
                  <a href="<?= site_url('cible_conservation/create') ?>"><button type="button" class="btn btn-block bg-gradient-success btn-sm">Nouvel espèce</button></a>
                  </div><!-- /.col -->
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Cible de conservation</th>
                    <th>Type</th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php foreach ($cible as $item):?>
                    <tr>
                      <td><?= $item['nom_cible_conservation'] ?></td>
                      <td><?= $item['type_cible_conservation'] ?></td>
                      <td><a href="<?= site_url('cible_conservation/detail/') ?><?= $item['id']?>">Voir</a> | <a href="<?= site_url('cible_conservation/edit/') ?><?= $item['id']?>">Modifier</a> | <a href="<?= site_url('cible_conservation/delete/') ?><?= $item['id']?>">Supprimer</a></td>
                    </tr>
                  <?php endforeach;?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

<?= $this->endSection('') ?>