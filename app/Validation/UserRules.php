<?php 

namespace App\Validation;
use App\Models\AdministrateurModel;

class UserRules
{
    public function validateUser(string $str, string $fields, array $data){
        $model = new AdministrateurModel();
        $user = $model->where('utilisateur', $data['utilisateur'])
                      ->first();
        if(!$user){
            return false;
        }

        return password_verify($data['password'], $user['password']);
    }
}