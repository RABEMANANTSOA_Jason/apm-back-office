<?php

namespace App\Models;

use CodeIgniter\Model;

class PopulationModel extends Model
{
    protected $table = 'Population';
    protected $primaryKey = 'id_population';
    protected $allowedFields = ['nom_population', 'description','image_population','impacts','id_aire_protegee'];
    protected $rules = [
        'nom_population' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez donner un nom pour la population",
            ]
        ]
    ];
}
