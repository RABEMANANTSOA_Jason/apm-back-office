<?php

namespace App\Models;

use CodeIgniter\Model;

class Mode_GestionModel extends Model
{
    protected $table = 'Mode_Gestion';
    protected $primaryKey = 'id';
    protected $allowedFields = ['type_mode_gestion', 'description'];
    protected $rules = [
        'type_mode_gestion' => [
            'rules' => 'required|is_unique[mode_gestion.type_mode_gestion]',
            'errors' => [
                'required' => "Veuillez donner un nom au type de gestion",
                'is_unique' => "Ce nom existe déjà"
            ]
        ],
    ];

    protected $update_rules = [
        'type_mode_gestion' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez donner un nom au type de gestion",
            ]
        ],
    ];
}
