<?php

namespace App\Models\custom;

use CodeIgniter\Database\ConnectionInterface;

class AireProtegeeCustomModel
{
    protected $db;

    public function __construct(ConnectionInterface &$db)
    {
        $this->db =& $db;
    }

    public function getAPindex(){
        $builder = $this->db->table('Aires_Protegees');
        $builder->join('Statut_Categorie_IUCN', 'Aires_Protegees.id_statut_categorie_iucn = Statut_Categorie_IUCN.id');
        $builder->join('Gestionnaire','Aires_Protegees.id_gestionnaire = Gestionnaire.id');
        $builder->join('Mode_Gestion','Aires_Protegees.id_mode_gestion = Mode_Gestion.id');
        $result = $builder->get()->getResult();
        return $result;
    }

    public function verificationCible($id){
        $builder = $this->db->table('Cible_Conservation');
        $builder->where(['Cible_Conservation.id_aire_protegee' =>$id]);
        return count($builder->get()->getResult());
    }

    public function verificationPopulation($id){
        $builder = $this->db->table('Population');
        $builder->where(['Population.id_aire_protegee' =>$id]);
        return count($builder->get()->getResult());
    }

    public function verificationImages($id){
        $builder = $this->db->table('Images');
        $builder->where(['Images.id_aire_protegee' =>$id]);
        return count($builder->get()->getResult());
    }

    public function getAPDetail($id){
        $builder = $this->db->table('Aires_Protegees');
        $builder->where(['Aires_Protegees.id_ap' => $id]);
        $builder->join('Statut_Categorie_IUCN', 'Aires_Protegees.id_statut_categorie_iucn = Statut_Categorie_IUCN.id');
        $builder->join('Gestionnaire','Aires_Protegees.id_gestionnaire = Gestionnaire.id');
        $builder->join('Mode_Gestion','Aires_Protegees.id_mode_gestion = Mode_Gestion.id');
        $builder->join('Geolocalisation','Geolocalisation.id_aire_protegee = Aires_Protegees.id_ap');
        if($this->verificationCible($id)>0){
            $builder->join('Cible_Conservation','Cible_Conservation.id_aire_protegee = Aires_Protegees.id_ap');
        }
        if($this->verificationPopulation($id)>0){
            $builder->join('Population','Population.id_aire_protegee = Aires_Protegees.id_ap');
        }
        if($this->verificationImages($id)>0){
            $builder->join('Images','Images.id_aire_protegee = Aires_Protegees.id_ap');
        }
        $result = $builder->get()->getResult();
        return $result;
    }

    public function beforeInsert(array $data){
        if($data['regions'] != null){
            $data['regions'] = implode(";",$data['regions']);
        }
        if($data['districts'] != null){
            $data['districts'] = implode(";",$data['districts']);
        }
        if($data['communes'] != null){
            $data['communes'] = implode(";",$data['communes']);
        }
        if($data['fokontany'] != null){
            $data['fokontany'] = implode(";",$data['fokontany']);
        }
        return $data;
    }

    public function beforeInsertGeoJSON(array $data, $id, $image){
        try{
            $json = json_decode($data['geojson']);
            $json->properties->Id = $id;
            if($image != null){
            $json->properties->image = $image;}

            $data['geojson'] = json_encode($json);
            return $data;
        }
        catch(\Exception $e){
            throw new \Exception("Veuillez fournir un des données de géolocalisation valides");
        }
    }

    public function insertAireProtegee(array $ap, array $geo){
        $this->db->transStart();
        $builder = $this->db->table('Aires_Protegees');
        $builder->insert($this->beforeInsert($ap));
        $id = $this->db->insertID();

        $geo['id_aire_protegee'] = $id;
        $builder = $this->db->table('Geolocalisation');
        $builder->insert($this->beforeInsertGeoJSON($geo,$id,$ap['ap_image']));
        $this->db->transComplete();
        return $id;
    }

    public function getGeolocalisationDetai($id){
        $builder = $this->db->table('Geolocalisation');
        $builder->where(['Geolocalisation.id_aire_protegee' => $id]);
        $result = $builder->get()->getResult();
        return $result;
    }

    public function updateAireProtegee(array $ap,$id_ap, array $geo, $id_geo){
        $this->db->transStart();
        $builder = $this->db->table('Aires_Protegees');
        $builder->where('id_ap', $id_ap);
        $builder->update($this->beforeInsert($ap));

        $builder = $this->db->table('Geolocalisation');
        $builder->where('id',$id_geo);
        $builder->update($this->beforeInsertGeoJSON($geo,$id_ap,$ap['ap_image']));
        $this->db->transComplete();
        return $id_ap;
    }
}
