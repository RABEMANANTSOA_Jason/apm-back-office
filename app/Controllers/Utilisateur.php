<?php namespace App\Controllers;

use \App\Libraries\Oauth;
use \OAuth2\Request;
use \CodeIgniter\API\ResponseTrait;
use App\Models\AdministrateurModel;

class Utilisateur extends BaseController
{
	public function index()
	{
		helper(['form']);
		return view('utilisateur/index');
	}

	public function login()
	{
		$data = [];
		helper(['form']);

		if($this->request->getMethod() == 'post'){
			$model = new AdministrateurModel();
			if(!$this->validate($model->rules_login)){
				$data['validation'] = $this->validator;
			}
			else{
				$user = $model->where('utilisateur', $this->request->getVar('utilisateur'))
							  ->first();
				$this->setUserSession($user);
				return redirect()->to('/dashboard');
			}
		}
		return view('utilisateur/index',$data);
	}

	public function setUserSession($user){
		$data = [
			'id' => $user['id'],
			'nom' => $user['nom'],
			'prenom' => $user['prenom'],
			'utilisateur' => $user['utilisateur'],
			'isLoggedIn'=> true,
		];

		session()->set($data);
		return true;
	}

	public function ajout(){
		$data = [];
		helper(['form']);

		if($this->request->getMethod() == 'post'){
			$model = new AdministrateurModel();
			if(!$this->validate($model->rules_add_edit)){
				$data['validation'] = $this->validator;
				/* 
				<? php if (isset($validation)) : ?>
					<div class="col-12">
						<div class="alert-danger" role="alert">
							<?= $validation->listErrors() ?>
						</div>
					</div>
				<?php endif; ?>
				*/
			}else{
				$newData = [
					'nom' => $this->request->getVar('nom'),
					'prenom' => $this->request->getVar('prenom'),
					'utilisateur' => $this->request->getVar('utilisateur'),
					'password' => $this->request->getVar('password'),
				];
				$model->save($newData);
				$session = session();
				$session->setFlashData('success', 'Utilisateur enregistré');
				return redirect()->to('/');
			}
		}else{
			//return register view
		}



		//return view('');
		
	}

	public function logout()
	{
		session()->destroy();
		return redirect()->to('/');
	}
	//--------------------------------------------------------------------

}
