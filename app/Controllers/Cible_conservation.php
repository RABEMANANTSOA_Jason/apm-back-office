<?php namespace App\Controllers;

use App\Models\CibleConservationModel;
use App\Models\AireProtegeModel;
use App\Models\custom\CibleConservationCustomModel;

class Cible_conservation extends BaseController
{
    private $path = '/images/cb/';
    private $relativepath = './images/cb';

    public function index()
    {
        $data = [];
        helper(['form']);
        $model = new CibleConservationModel();
        $data['cible'] = $model->findAll();
        /*echo '<pre>';
            print_r($data['actualites']);
        echo '<pre>';*/
        return view('cible_conservation/index',$data);
    }

    public function create(){
        $data = [];
        helper(['form']);
        $model = new AireProtegeModel();
        $data['ap'] = $model->findAll();
        return view('cible_conservation/add',$data);
    }

    public function add()
    {
        $data = [];
        helper(['form']);
        $model = new CibleConservationModel();        
        if (!$this->validate($model->rules)) {
            $aire_protege = new AireProtegeModel();
            $data['ap'] = $aire_protege->findAll();
            $data['validation'] = $this->validator;
        } else {
            $file = $this->request->getFile('image_cible');
            $insert = [
                'nom_cible_conservation' => $this->request->getVar('nom_cible_conservation'),
                'type_cible_conservation' => $this->request->getVar('type_cible_conservation'),
                'id_aire_protegee' => $this->request->getVar('id_aire_protegee'),
                'description_cible_conservation' => $this->request->getVar('description_cible_conservation'),
            ];

            if($file->isValid() && !$file->hasMoved()){
                $file->move($this->relativepath);
                $insert['image_cible'] = $this->path.$file->getName();
            }
            $id = $model->insert($insert);
            $session = session();
            $session->setFlashData('success', 'Cible de conservation ajoutée : '.$insert['nom_cible_conservation']);
            return redirect()->to("/cible_conservation/index");
        }
        return view('cible_conservation/add',$data);
    }

    public function detail($id = NULL){
        $data = [];
        $db = db_connect();
        $model = new CibleConservationCustomModel($db);
        $data['cible'] = $model->getDetailCible($id);
        if($data['cible']){
            return view('cible_conservation/detail',$data);
        }
        else{
            $session = session();
			$session->setFlashData('error', "Cible de conservation non trouvée");
            return redirect()->to("/cible_conservation/index");
        }
    }

    public function edit($id = NULL){
        $data = [];
        helper(['form']);
        $model = new CibleConservationModel();
        $data['cible'] = $model->find($id);
        if($data['cible']){
            $ap = new AireProtegeModel();
            $data['ap'] = $ap->findAll();
            return view('cible_conservation/edit',$data);
        }
        else{
            $session = session();
			$session->setFlashData('error', "Cible de conservation non trouvée");
            return redirect()->to("/cibleconservation/index");
        }
    }

    public function update($id = null)
    {
        $data = [];
        helper(['form']);
        $model = new CibleConservationModel();
        $data['cible'] = $model->find($id);
        if($data['cible']){
            if (!$this->validate($model->rules)) {
                $aire_protege = new AireProtegeModel();
                $data['ap'] = $aire_protege->findAll();
                $data['validation'] = $this->validator;
                $data['cible']['id'] = $id;
            } else {
                $image = $data['cible']['image_cible'];
                $file = $this->request->getFile('image_cible');

                $update = [
                    'id' => $id,
                    'nom_cible_conservation' => $this->request->getVar('nom_cible_conservation'),
                    'type_cible_conservation' => $this->request->getVar('type_cible_conservation'),
                    'id_aire_protegee' => $this->request->getVar('id_aire_protegee'),
                    'description_cible_conservation' => $this->request->getVar('description_cible_conservation'),
                ];

                if($file->isValid() && !$file->hasMoved()){
                    $file->move($this->relativepath);
                    if(file_exists("./".$image)){
                        unlink("./".$image);
                    }
                    $update['image_cible'] = $this->path.$file->getName();;
                }
                $model->save($update);
                $session = session();
                $session->setFlashData('success', 'Cible de conservation modifiée');
                return redirect()->to("/cibleconservation/detail/".$id);
            }
            return view('cible_conservation/edit',$data);
        }else{
            $session = session();
            $session->setFlashData('error', "Cible de conservation non trouvée");
            return redirect()->to("/cibleconservation/index");
        }
    }

    public function delete($id = null)
    {
        $model = new CibleConservationModel();
        $data = $model->find($id);
        if ($data) {
            $nom = $data['nom_cible_conservation'];
            $image = $data['image_cible'];
            $model->delete($id);
            if(file_exists("./".$image)){
                unlink("./".$image);
            }
            $session = session();
			$session->setFlashData('success', "Cible de conservation : '".$nom."' a été supprimée");
            return redirect()->to("/cibleconservation/index");
        } else {
            $session = session();
			$session->setFlashData('error', "Cible de conservation non trouvée");
            return redirect()->to("/cibleconservation/index");
        }
    }
}
