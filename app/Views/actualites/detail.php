<?= $this->extend('templates/admin_template') ?>

<?= $this->section('content') ?>

<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Actualités</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= site_url('/actualites') ?>">Accueil</a></li>
              <li class="breadcrumb-item active">Actualité détail</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
        <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-sm-10">
            <h3 class="card-title">
                <i class="fas fa-newspaper"></i>
                <?= $actualite['titre_actualite'] ?>
            </h3>
            </div>
            <div class="col-sm-2">
            <a href="<?= site_url('actualites/edit/') ?><?= $actualite['id']?>"><button type="button" class="btn btn-block bg-gradient-success btn-sm">Modifier</button></a>
            </div>
          </div>
          </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="row">
                <div class="col-sm-3"></div>
                <div class="col-sm-6">
                  <?php if($actualite['image_actualite'] != null):?>
                    <img class="img-fluid" src="<?= $actualite['image_actualite'] ?>" alt="Photo">
                  <?php endif;?>
                </div>
                <div class="col-sm-3"></div>
            </div>
            <blockquote>
                <p><?= $actualite['contenu_actualite'] ?></p>
                <small>Auteur <cite title="Source Title"><?= $actualite['auteur_actualite'] ?></cite></small><br>
                <small><cite title="Source Title"><?php $orgDate = $actualite['date_publication'];$newDate = date("d-m-Y", strtotime($orgDate));echo $newDate;?>              
                </cite></small>
            </blockquote>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
      </div><!-- /.container-fluid -->
    </div>

<?= $this->endSection('') ?>