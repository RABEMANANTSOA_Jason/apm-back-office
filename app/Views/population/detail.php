<?= $this->extend('templates/admin_template') ?>

<?= $this->section('content') ?>

<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Population détail</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= site_url('/population') ?>">Accueil</a></li>
              <li class="breadcrumb-item active">Détail</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
        <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-sm-10">
            <h3 class="card-title">
                <i class="fas fa-newspaper"></i>
                <?= $population[0]->nom_population ?>
            </h3>
            </div>
            <div class="col-sm-2">
            <a href="<?= site_url('population/edit/') ?><?= $population[0]->id_population?>"><button type="button" class="btn btn-block bg-gradient-success btn-sm">Modifier</button></a>
            </div>
          </div>
          </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="row">
                <div class="col-sm-3"></div>
                <div class="col-sm-6">
                  <?php if($population[0]->image_population != null):?>
                    <img class="img-fluid" src="<?= $population[0]->image_population ?>" alt="Photo">
                  <?php endif;?>
                </div>
                <div class="col-sm-3"></div>
            </div>
            <blockquote>
                <b>Description</b>
                <p><?= $population[0]->description ?></p>
                <b>Impacts</b>
                <p><?= $population[0]->impacts ?></p>
                <b>Localisation</b>
                <p><?= $population[0]->nom_du_site?></p>
            </blockquote>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
      </div><!-- /.container-fluid -->
    </div>

<?= $this->endSection('') ?>