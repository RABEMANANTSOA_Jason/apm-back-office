<?php

namespace App\Models;

use CodeIgniter\Model;

class GestionnaireModel extends Model
{
    protected $table = 'Gestionnaire';
    protected $primaryKey = 'id';
    protected $allowedFields = ['nom_gestionnaire', 'description_gestionnaire', 'date_creation_gestionnaire'];
    protected $rules = [
        'nom_gestionnaire' => [
            'rules' => 'required|is_unique[gestionnaire.nom_gestionnaire]',
            'errors' => [
                'required' => "Veuillez donner un nom au gestionnaire",
                'is_unique' => "Ce nom existe déjà"
            ]
        ],
    ];
    protected $update_rules = [
        'nom_gestionnaire' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez donner un nom au gestionnaire",
            ]
        ],
    ];
}
