<?= $this->extend('templates/admin_template') ?>

<?= $this->section('content') ?>

<!-- Content Header (Page header) -->
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Visite virtuelle</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= site_url('/visite_virtuelle') ?>">Accueil</a></li>
              <li class="breadcrumb-item active">Modification</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Modification d'une visite virtuelle</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
              </button>
            </div>
          </div>
          <?php if (isset($validation)) : ?>
          <div class="row">
            <div class="col-12">
              <div class="card card-danger direct-chat direct-chat-danger shadow-lg">
                <div class="card-header">
                  <h3 class="card-title">Erreur(s)</h3>
                </div>
                <div class="card-body">
                  <?= $validation->listErrors() ?>
                </div>
              </div>
            </div>
          </div>
        <?php endif; ?>
          <!-- /.card-header -->
          <div class="card-body">
            <!-- /.col (left) -->
            <form action="<?= site_url('visite_virtuelle/update/') ?><?= $visite['id']?>" method="post" enctype='multipart/form-data'>

          <div class="col-md-12">
            <div class="card card-success">
                <div class="card-header">
                    <h3 class="card-title">Détails de la visite</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Nom de la visite</label>
                                <input type="text" class="form-control" name="nom_visite" placeholder="Nom de la visite" value="<?= $visite['nom_visite'] ?>">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Aire protégés</label>
                                <div class="form-group">
                                <select class="form-control select2bs4" name="id_aire_protegee" style="width: 100%;">
                                    <?php foreach ($liste_ap as $item):?>
                                        <?php if($item['id_ap'] == $visite['id_aire_protegee']): ?>
                                        <option value="<?= $item['id_ap'] ?>" <?= set_select('id_mode_gestion', $item['id_ap']); ?> selected><?= $item['nom_du_site'] ?></option>
                                        <?php else: ?>
                                            <option value="<?= $item['id_ap'] ?>" <?= set_select('id_mode_gestion', $item['id_ap']); ?>><?= $item['nom_du_site'] ?></option>
                                        <?php endif; ?>
                                    <?php endforeach;?>
                                </select>
                              </div>
                            </div>
                        </div>
                                            
                    </div>
                </div>
            </div>
            
              <div class="form-group">
                <label>Image (Nord)</label>
                    <div class="custom-file">
                    <input type="file" class="custom-file-input" id="image_nord" name="image_nord" multiple>
                    <label class="custom-file-label" for="exampleInputFile">Importer une image</label>
                </div>
            </div>
            <div class="form-group">
                <label>Image (Ouest)</label>
                    <div class="custom-file">
                    <input type="file" class="custom-file-input" id="image_ouest" name="image_ouest" multiple>
                    <label class="custom-file-label" for="exampleInputFile">Importer une image</label>
                </div>
            </div>
            <div class="form-group">
                <label>Image (Centre)</label>
                    <div class="custom-file">
                    <input type="file" class="custom-file-input" id="image_centre" name="image_centre" multiple>
                    <label class="custom-file-label" for="exampleInputFile">Importer une image</label>
                </div>
            </div>
            <div class="form-group">
                <label>Image (Est)</label>
                    <div class="custom-file">
                    <input type="file" class="custom-file-input" id="image_est" name="image_est" multiple>
                    <label class="custom-file-label" for="exampleInputFile">Importer une image</label>
                </div>
            </div>
            <div class="form-group">
                <label>Image (Sud)</label>
                    <div class="custom-file">
                    <input type="file" class="custom-file-input" id="image_sud" name="image_sud" multiple>
                    <label class="custom-file-label" for="exampleInputFile">Importer une image</label>
                </div>
            </div>
          </div>
          <div class="col-md-12">
            <button type="submit" class="btn btn-block btn-success btn-lg">Enregistrer la visite</button>
          </div>
            </form>
        </div>
        <!-- /.card -->

        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    

<?= $this->endSection('') ?>