<?php

namespace App\Models;

use CodeIgniter\Model;

class ActualitesModel extends Model
{
    protected $table = 'Actualites';
    protected $primaryKey = 'id';
    protected $allowedFields = ['titre_actualite', 'date_publication', 'contenu_actualite', 'image_actualite','auteur_actualite'];
    protected $rules = [
        'titre_actualite' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez donner un titre à l'actualité",
            ]
        ],
        'contenu_actualite' => [
            'rules' => 'required|min_length[2000]',
            'errors' => [
                'required' => "Veuillez fournir un contenu à l'actualité",
                'min_length' => "Votre article est en dessous des 2000 caractères"
            ]
        ],
        'image_actualite' => [
            'rules' => 'is_image[image_actualite]',
            'errors' => [
                'uploaded' => "Veuillez enregistrer une image",
                'is_image' => "Veuillez fournir une image valide"
            ]
        ],
        'auteur_actualite' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez donner l'auteur de l'actualité",
            ]
        ]
    ];

    protected $rulesUpdate = [
        'titre_actualite' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez donner un titre à l'actualité",
            ]
        ],
        'contenu_actualite' => [
            'rules' => 'required|min_length[2000]',
            'errors' => [
                'required' => "Veuillez fournir un contenu à l'actualité",
                'min_length' => "Votre article est en dessous des 2000 caractères"
            ]
        ],
        'image_actualite' => [
            'rules' => 'is_image[image_actualite]',
            'errors' => [
                'is_image' => "Veuillez fournir une image valide"
            ]
        ],
        'auteur_actualite' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez donner l'auteur de l'actualité",
            ]
        ]
    ];

    public function getActualites(){
        return $this->orderBy('date_publication', 'desc')->orderBy('id','desc')->findAll();
    }
}
