<?php

namespace App\Models;

use CodeIgniter\Model;

class CibleConservationModel extends Model
{
    protected $table = 'Cible_Conservation';
    protected $primaryKey = 'id';
    protected $allowedFields = ['type_cible_conservation', 'nom_cible_conservation','description_cible_conservation','image_cible','id_aire_protegee'];
    protected $rules = [
        'type_cible_conservation' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez définir s'il sagit de faune ou flore",
            ]
        ],
        'nom_cible_conservation' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez donner un nom à l'espèce",
            ]
        ]
    ];
}
