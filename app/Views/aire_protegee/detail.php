<?= $this->extend('templates/admin_template') ?>

<?= $this->section('content') ?>

<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Aires protégées</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= site_url('/aire_protegee') ?>">Accueil</a></li>
              <li class="breadcrumb-item active">Aires protégées</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

    <!-- /.content-header -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- Main content -->
            <div class="invoice p-3 mb-3">
              <!-- title row -->
              <div class="row">
                <div class="col-12">
                  <h4>
                    <i class="fas fa-leaf"></i> <?= $aire_protegee[0]->nom_du_site ?>
                    <small class="float-right">Date de création: <?php $orgDate = $aire_protegee[0]->date_creation_ap;$newDate = date("d-m-Y", strtotime($orgDate));echo $newDate;?></small>
                  </h4>
                  <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-6">
                    <?php if($aire_protegee[0]->ap_image != null):?>
                    <img class="img-fluid" src="<?= base_url()?><?=($aire_protegee[0]->ap_image) ?>" alt="Photo">
                  <?php endif;?>
                    </div>
                    <div class="col-sm-3"></div>
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- info row -->
              <div class="row invoice-info">
                <div class="col-sm-6 invoice-col">
                    <br>
                    <address>
                        <strong>Catégorie: </strong> <?= $aire_protegee[0]->categorie ?> <br> 
                        <strong>Statut: </strong> <?= $aire_protegee[0]->statut_fr ?><br> 
                        <strong>Type d'aire protégée: </strong> <?= $aire_protegee[0]->type_ap ?><br> 
                    </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-6 invoice-col">
                  <br>
                  <address>
                    <strong>Gestionnaire: </strong> <?= $aire_protegee[0]->nom_gestionnaire ?><br> 
                    <strong>Mode de gestion: </strong> <?= $aire_protegee[0]->type_mode_gestion ?><br> 
                  </address>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- Table row -->
              <div class="row">
                <div class="col-12">
                  <!-- Custom Tabs -->
            <div class="card">
              <div class="card-header d-flex p-0">
                <h3 class="card-title p-3">Concernant le site</h3>
                <ul class="nav nav-pills ml-auto p-2">
                  <li class="nav-item"><a class="nav-link active" href="#tab_1" data-toggle="tab">Description</a></li>
                  <li class="nav-item"><a class="nav-link" href="#tab_2" data-toggle="tab">Informations géographiques</a></li>
                  <li class="nav-item"><a class="nav-link" href="#tab_3" data-toggle="tab">Arrêtés et décrets</a></li>
                  <li class="nav-item"><a class="nav-link" href="#tab_4" data-toggle="tab">Cibles de conservation</a></li>
                  <li class="nav-item"><a class="nav-link" href="#tab_5" data-toggle="tab">Menaces</a></li>
                  <li class="nav-item"><a class="nav-link" href="#tab_6" data-toggle="tab">Potentialités</a></li>
                  <li class="nav-item"><a class="nav-link" href="#tab_7" data-toggle="tab">Population locale</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
                    <?= $aire_protegee[0]->historique ?>
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_2">
                    <div class="table-responsive">
                      <table class="table">
                        <tr>
                          <th style="width:50%">Superficie Dossier suivant le decret (Ha)</th>
                          <td><?= $aire_protegee[0]->superficie_decret ?></td>
                        </tr>
                        <tr>
                          <th>Superficie ND (Ha)</th>
                          <td><?= $aire_protegee[0]->superficie_nd ?></td>
                        </tr>
                        <tr>
                          <th>Superficie ZT</th>
                          <td><?= $aire_protegee[0]->superficie_zt ?></td>
                        </tr>
                      </table>
                    </div>
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_3">
                    <div class="table-responsive">
                        <table class="table">
                          <tr>
                            <th style="width:50%">N°Arrété de mise en protection temporaire</th>
                            <td><?= $aire_protegee[0]->arrete_mise_en_protection ?></td>
                          </tr>
                          <tr>
                            <th>Date de mise en protection temporaire</th>
                            <td><?php if($aire_protegee[0]->date_mise_en_protection != null):?>
                              <?php $orgDate = $aire_protegee[0]->date_mise_en_protection;$newDate = date("d-m-Y", strtotime($orgDate));echo $newDate;?>
                            <?php endif;?></td>
                          </tr>
                          <tr>
                            <th>Arrêté/Décret en cours</th>
                            <td><?= $aire_protegee[0]->arrete_decret_en_cours ?></td>
                          </tr>
                          <tr>
                            <th>N°décret</th>
                            <td><?= $aire_protegee[0]->numero_decret ?></td>
                          </tr>
                          <tr>
                            <th>Date de signature</th>
                            <td><?php if($aire_protegee[0]->date_signature != null):?>
                              <?php $orgDate = $aire_protegee[0]->date_signature;$newDate = date("d-m-Y", strtotime($orgDate));echo $newDate;?>
                              <?php endif;?></td>
                          </tr>
                        </table>
                      </div>
                  </div>
                  <div class="tab-pane" id="tab_4">
                    <div class="card-body">
                      <ul>
                      <?php foreach($cible_conservation as $item):?>
                        <li><?= $item->nom_cible_conservation?></li>
                      <?php endforeach;?>
                      </ul>
                    </div>
                  </div>
                  <div class="tab-pane" id="tab_5">
                    <div class="card-body">
                      <dl>
                        <dt>Les menaces</dt>
                        <?= $aire_protegee[0]->menaces ?>
                      </dl>
                    </div>
                  </div>
                  <div class="tab-pane" id="tab_6">
                    <div class="card-body">
                      <?= $aire_protegee[0]->potentialites ?>
                    </div>
                  </div>
                  <div class="tab-pane" id="tab_7">
                    <div class="card-body">
                      <dl>
                        <dt>Impact</dt>
                        <dd>Population</dd>
                        <dt>Description</dt>
                        <dt>Image</dt>
                      </dl>
                    </div>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- ./card -->
            </div>
            </div>
            <!-- /.invoice -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
<?= $this->endSection('') ?>