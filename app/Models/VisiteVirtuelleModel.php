<?php

namespace App\Models;

use CodeIgniter\Model;

class VisiteVirtuelleModel extends Model
{
    protected $table = 'Visite_Virtuelle';
    protected $primaryKey = 'id';
    protected $allowedFields = ['nom_visite', 'image_nord', 'image_est', 'image_centre','image_ouest','image_sud', 'id_aire_protegee'];
    protected $rules = [
        'nom_visite' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez donner un titre à la visite",
            ]
        ],
        'image_nord' => [
            'rules' => 'is_image[image_nord]',
            'errors' => [
                'is_image' => "Veuillez fournir une image valide pour le nord"
            ]
        ],
        'image_ouest' => [
            'rules' => 'is_image[image_ouest]',
            'errors' => [
                'is_image' => "Veuillez fournir une image valide pour l'ouest"
            ]
        ],
        'image_centre' => [
            'rules' => 'uploaded[image_centre]|is_image[image_centre]',
            'errors' => [
                'uploaded' => "Veuillez fournir au moins une image centre",
                'is_image' => "Veuillez fournir une image valide pour le centre"
            ]
        ],
        'image_est' => [
            'rules' => 'is_image[image_est]',
            'errors' => [
                'is_image' => "Veuillez fournir une image valide pour l'est"
            ]
        ],
        'image_sud' => [
            'rules' => 'is_image[image_sud]',
            'errors' => [
                'is_image' => "Veuillez fournir une image valide pour le sud"
            ]
        ]
    ];
    protected $rules_update = [
        'nom_visite' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez donner un titre à la visite",
            ]
        ],
        'image_nord' => [
            'rules' => 'is_image[image_nord]',
            'errors' => [
                'is_image' => "Veuillez fournir une image valide pour le nord"
            ]
        ],
        'image_ouest' => [
            'rules' => 'is_image[image_ouest]',
            'errors' => [
                'is_image' => "Veuillez fournir une image valide pour l'ouest"
            ]
        ],
        'image_centre' => [
            'rules' => 'is_image[image_centre]',
            'errors' => [
                'is_image' => "Veuillez fournir une image valide pour le centre"
            ]
        ],
        'image_est' => [
            'rules' => 'is_image[image_est]',
            'errors' => [
                'is_image' => "Veuillez fournir une image valide pour l'est"
            ]
        ],
        'image_sud' => [
            'rules' => 'is_image[image_sud]',
            'errors' => [
                'is_image' => "Veuillez fournir une image valide pour le sud"
            ]
        ]
    ];

    
}
