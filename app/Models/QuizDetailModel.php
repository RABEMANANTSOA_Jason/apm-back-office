<?php

namespace App\Models;

use CodeIgniter\Model;

class QuizDetailModel extends Model
{
    protected $table = 'Quiz_detail';
    protected $primaryKey = 'id';
    protected $allowedFields = ['question,optionA,optionB,optionC,optionD,reponse'];
    protected $rules = [
        'nom_quiz' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez fournir un titre au quiz",
            ]
        ],
        'question' => [
            'rules' => 'required|is_unique[Quiz_detail.question]',
            'errors' => [
                'required' => "Veuillez fournir une question",
                'is_unique' => "Cette question existe déjà"
            ]
        ],
        'optionA' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez fournir une première option",
            ]
        ],
        'optionB' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez fournir une deuxième option",
            ]
        ],
        'optionC' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez fournir une troisième option",
            ]
        ],
        'optionD' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez fournir une quatrième option",
            ]
        ],
        'reponse' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez fournir une réponse"
            ]
        ]
    ];

    protected $rules_question = [
        'question' => [
            'rules' => 'required|is_unique[Quiz_detail.question]',
            'errors' => [
                'required' => "Veuillez fournir une question",
                'is_unique' => "Cette question existe déjà"
            ]
        ],
        'optionA' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez fournir une première option",
            ]
        ],
        'optionB' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez fournir une deuxième option",
            ]
        ],
        'optionC' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez fournir une troisième option",
            ]
        ],
        'optionD' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez fournir une quatrième option",
            ]
        ],
        'reponse' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez fournir une réponse"
            ]
        ]
    ];
}