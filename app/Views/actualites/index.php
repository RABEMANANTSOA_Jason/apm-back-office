<?= $this->extend('templates/admin_template') ?>

<?= $this->section('content') ?>

<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Actualités</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= ('dashboard') ?>">Accueil</a></li>
              <li class="breadcrumb-item active">Actualités</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Timelime example  -->
        <div class="row">
          <div class="col-md-12">
            <!-- The time line -->
            <div class="timeline">
                <!-- timeline time label -->
                <div class="time-label">
                    <span class="bg-blue">Rédiger un article</span>
                </div>
                <div>
                <i class="fas fa-plus bg-green"></i>
                    <div class="timeline-item">
                    <form action="<?= site_url('actualites/add')?>" method="post" enctype="multipart/form-data">

                      <h3 class="timeline-header">
                        <input type="text" class="form-control" name="titre_actualite" placeholder="Titre de l'article" value="<?= set_value('titre_actualite') ?>">
                      </h3>
                      <div class="timeline-body">
                        <textarea class="form-control" name="contenu_actualite" rows="3" placeholder="Rédiger ici (2000 caractères minimum)"><?= set_value('contenu_actualite') ?></textarea>
                        <div class="custom-file">
                          <input type="file" class="custom-file-input" id="image_actualite" name="image_actualite" value="<?= set_value('image_actualite') ?>" onchange="readURL(this);">
                          <label class="custom-file-label" for="exampleInputFile">Importer une image</label>
                        </div>
                        <img id="blah" class="img-fluid" src="#" alt="Votre image" />
                      </div>
                      <br/>
                      <div class="timeline-footer">
                        <input type="text" class="form-control" name="auteur_actualite" placeholder="Auteur" value="<?= set_value('auteur_actualite') ?>">
                        <br/>
                        <?php if (isset($validation)) : ?>
                          <div class="row">
                            <div class="col-12">
                              <div class="card card-danger direct-chat direct-chat-danger shadow-lg">
                                <div class="card-header">
                                  <h3 class="card-title">Erreur(s)</h3>
                                </div>
                                <div class="card-body">
                                  <?= $validation->listErrors() ?>
                                </div>
                              </div>
                            </div>
                          </div>
                        <?php endif; ?>
                        <br/>
                        <button type="submit" class="btn btn-success btn-sm">Publier</button>
                      </div>
                      </form> 
                    </div>
                  </div>
                <!-- timeline time label -->
                <?php $date; $i = 0; ?>
                <?php foreach ($actualites as $item):?>
                    <?php if($i === 0): ?>
                      <?php $date = $item['date_publication']; ?>
                      <div class="time-label">
                          <span class="bg-red"><?php $orgDate = $item['date_publication'];$newDate = date("d-m-Y", strtotime($orgDate));echo $newDate;?> </span>
                      </div>
                      <div>
                          <i class="fas fa-newspaper bg-blue"></i>
                          <div class="timeline-item">
                            <h3 class="timeline-header"><?= $item['titre_actualite'] ?></h3>
                            <div class="timeline-body">
                              <?= substr_replace($item['contenu_actualite']," ...", 250) ?>
                            </div>
                            <div class="timeline-footer">
                                <a href="<?= site_url('actualites/detail/') ?><?= $item['id']?>" class="btn btn-primary btn-sm">Lire</a>
                                <a href="<?= site_url('actualites/delete/') ?><?= $item['id']?>" class="btn btn-danger btn-sm">Supprimer</a>
                            </div>
                          </div>
                      </div>
                      <?php $i++; ?>

                    <?php elseif($i > 0 && $date != $item['date_publication']):?>
                      <?php $date = $item['date_publication']; ?>
                      <div class="time-label">
                          <span class="bg-red"><?php $orgDate = $item['date_publication'];$newDate = date("d-m-Y", strtotime($orgDate));echo $newDate;?></span>
                      </div>
                      <div>
                          <i class="fas fa-newspaper bg-blue"></i>
                          <div class="timeline-item">
                            <h3 class="timeline-header"><?= $item['titre_actualite'] ?></h3>
                            <div class="timeline-body">
                              <?= substr_replace($item['contenu_actualite']," ...", 250) ?>
                            </div>
                            <div class="timeline-footer">
                                <a href="<?= site_url('actualites/detail/') ?><?= $item['id']?>" class="btn btn-primary btn-sm">Lire</a>
                                <a href="<?= site_url('actualites/delete/') ?><?= $item['id']?>" class="btn btn-danger btn-sm">Supprimer</a>
                            </div>
                          </div>
                      </div>
                    <?php elseif($i > 0 && $date === $item['date_publication']): ?>
                      <div>
                          <i class="fas fa-newspaper bg-blue"></i>
                          <div class="timeline-item">
                            <h3 class="timeline-header"><?= $item['titre_actualite'] ?></h3>
                            <div class="timeline-body">
                              <?= substr_replace($item['contenu_actualite']," ...", 250) ?>
                            </div>
                            <div class="timeline-footer">
                                <a href="<?= site_url('actualites/detail/') ?><?= $item['id']?>" class="btn btn-primary btn-sm">Lire</a>
                                <a href="<?= site_url('actualites/delete/') ?><?= $item['id']?>" class="btn btn-danger btn-sm">Supprimer</a>
                            </div>
                          </div>
                      </div>
                    <?php endif; ?>
                  <!-- END timeline item -->
                <?php endforeach; ?>
              <div>
                <i class="fas fa-clock bg-gray"></i>
              </div>
            </div>
          </div>
          <!-- /.col -->
        </div>
      </div>
      <!-- /.timeline -->

    </section>
    <!-- /.content -->
    <script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    </script>
<?= $this->endSection('') ?>