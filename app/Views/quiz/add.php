<?= $this->extend('templates/admin_template') ?>

<?= $this->section('content') ?>

<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Quiz</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?= site_url('/quiz') ?>">Accueil</a></li>
                    <li class="breadcrumb-item active">Ajout</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">Nouveau quiz</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                </div>
            </div>
            <?php if (isset($validation)) : ?>
                <div class="row">
                    <div class="col-12">
                        <div class="card card-danger direct-chat direct-chat-danger shadow-lg">
                            <div class="card-header">
                                <h3 class="card-title">Erreur(s)</h3>
                            </div>
                            <div class="card-body">
                                <?= $validation->listErrors() ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <!-- /.card-header -->
            <div class="card-body">
                <!-- /.col (left) -->
                <form action="<?= site_url('quiz/add') ?>" method="post" enctype='multipart/form-data'>

                    <div class="col-md-12">
                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Information générale</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Titre du quiz</label>
                                            <input type="text" class="form-control" name="nom_quiz" placeholder="Titre du quiz" value="<?= set_value('nom_quiz') ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Question</label>
                                            <input type="text" class="form-control" name="question" placeholder="Première question" value="<?= set_value('question') ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Choix 1</label>
                            <input type="text" class="form-control" name="optionA" placeholder="Choix 1" value="<?= set_value('optionA') ?>">
                        </div>
                        <div class="form-group">
                            <label>Choix 2</label>
                            <input type="text" class="form-control" name="optionB" placeholder="Choix 2" value="<?= set_value('optionB') ?>">
                        </div>
                        <div class="form-group">
                            <label>Choix 3</label>
                            <input type="text" class="form-control" name="optionC" placeholder="Choix 3" value="<?= set_value('optionC') ?>">
                        </div>
                        <div class="form-group">
                            <label>Choix 4</label>
                            <input type="text" class="form-control" name="optionD" placeholder="Choix 4" value="<?= set_value('optionD') ?>">
                        </div>
                        <div class="row">
                            <div class="col-12"><label>Réponse</label></div>
                            <div class="col-2"></div>
                            <div class="col-2">
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="customRadio1" name="reponse" value="1" checked>
                                    <label for="customRadio1" class="custom-control-label">Choix 1</label>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="customRadio2" name="reponse" value="2">
                                    <label for="customRadio2" class="custom-control-label">Choix 2</label>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="customRadio3" name="reponse" value="3">
                                    <label for="customRadio3" class="custom-control-label">Choix 3</label>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="customRadio4" name="reponse" value="4">
                                    <label for="customRadio4" class="custom-control-label">Choix 4</label>
                                </div>
                            </div>
                            <div class="col-2"></div>
                        </div>
                    </div>
                    <br/>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-block btn-success btn-lg">Enregsitrer</button>
                    </div>
                </form>
            </div>
            <!-- /.card -->

            <!-- /.row -->
        </div><!-- /.container-fluid -->
</section>
<!-- /.content -->


<?= $this->endSection('') ?>