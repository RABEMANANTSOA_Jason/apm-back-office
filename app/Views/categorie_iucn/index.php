<?= $this->extend('templates/admin_template') ?>

<?= $this->section('content') ?>

<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Catégories IUCN</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= ('categorie_iucn') ?>">Accueil</a></li>
              <li class="breadcrumb-item active">Catégories</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-sm-10">
                    <h3 class="card-title">Les catégories des aires protégées</h3>
                  </div>
                  <div class="col-sm-2">
                  <a href="<?= site_url('categorie_iucn/create') ?>"><button type="button" class="btn btn-block bg-gradient-success btn-sm">Ajouter un gestionnaire</button></a>
                  </div><!-- /.col -->
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Catégorie IUCN</th>
                    <th>Code</th>
                    <th>Statut</th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php foreach ($categorie_iucn as $item):?>
                    <tr>
                      <td><?= $item['categorie'] ?></td>
                      <td><?= $item['code_statut_categorie_iucn'] ?></td>
                      <td><?= $item['statut_fr'] ?></td>
                      <td><a href="<?= site_url('categorie_iucn/detail/') ?><?= $item['id']?>">Voir</a> | <a href="<?= site_url('categorie_iucn/edit/') ?><?= $item['id']?>"">Modifier</a></td>
                    </tr>
                  <?php endforeach;?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

<?= $this->endSection('') ?>