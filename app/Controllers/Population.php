<?php namespace App\Controllers;

use App\Models\PopulationModel;
use App\Models\AireProtegeModel;
use App\Models\custom\PopulationCustomModel;

class Population extends BaseController
{
    private $path = '/images/population/';
    private $relativepath = './images/population';

    public function index()
    {
        $data = [];
        helper(['form']);
        $model = new PopulationModel();
        $data['population'] = $model->findAll();
        /*echo '<pre>';
            print_r($data['actualites']);
        echo '<pre>';*/
        return view('population/index',$data);
    }

    public function create(){
        $data = [];
        helper(['form']);
        $model = new AireProtegeModel();
        $data['ap'] = $model->findAll();
        return view('population/add',$data);
    }

    public function add()
    {
        $data = [];
        helper(['form']);
        $model = new PopulationModel();        
        if (!$this->validate($model->rules)) {
            $aire_protege = new AireProtegeModel();
            $data['ap'] = $aire_protege->findAll();
            $data['validation'] = $this->validator;
        } else {
            $file = $this->request->getFile('image_population');
            $insert = [
                'nom_population' => $this->request->getVar('nom_population'),
                'id_aire_protegee' => $this->request->getVar('id_aire_protegee'),
                'description' => $this->request->getVar('description'),
                'impacts' => $this->request->getVar('impacts'),
            ];

            if($file->isValid() && !$file->hasMoved()){
                $file->move($this->relativepath);
                $insert['image_population'] = $this->path.$file->getName();
            }
            $id = $model->insert($insert);
            $session = session();
            $session->setFlashData('success', 'Population ajoutée : '.$insert['nom_population']);
            return redirect()->to("/population/index");
        }
        return view('population/add',$data);
    }

    public function detail($id = NULL){
        $data = [];
        $db = db_connect();
        $model = new PopulationCustomModel($db);
        $data['population'] = $model->getDetailPopulation($id);
        if($data['population']){
            return view('population/detail',$data);
        }
        else{
            $session = session();
			$session->setFlashData('error', "Population non trouvée");
            return redirect()->to("/population/index");
        }
    }

    public function edit($id = NULL){
        $data = [];
        helper(['form']);
        $model = new PopulationModel();
        $data['population'] = $model->find($id);
        if($data['population']){
            $ap = new AireProtegeModel();
            $data['ap'] = $ap->findAll();
            return view('population/edit',$data);
        }
        else{
            $session = session();
			$session->setFlashData('error', "Population non trouvée");
            return redirect()->to("/population/index");
        }
    }

    public function update($id = null)
    {
        $data = [];
        helper(['form']);
        $model = new PopulationModel();
        $data['population'] = $model->find($id);
        if($data['population']){
            if (!$this->validate($model->rules)) {
                $aire_protege = new AireProtegeModel();
                $data['ap'] = $aire_protege->findAll();
                $data['validation'] = $this->validator;
            } else {
                $image = $data['population']['image_population'];
                $file = $this->request->getFile('image_population');

                $update = [
                    'id_population' => $id,
                    'nom_population' => $this->request->getVar('nom_population'),
                    'id_aire_protegee' => $this->request->getVar('id_aire_protegee'),
                    'description' => $this->request->getVar('description'),
                    'impacts' => $this->request->getVar('impacts'),
                ];

                if($file->isValid() && !$file->hasMoved()){
                    $file->move($this->relativepath);
                    if(file_exists("./".$image) && !is_dir("./".$image)){
                        unlink("./".$image);
                    }
                    $update['image_population'] = $this->path.$file->getName();;
                }
                $model->save($update);
                $session = session();
                $session->setFlashData('success', 'Population modifiée');
                return redirect()->to("/population/detail/".$id);
            }
            return view('population/edit',$data);
        }else{
            $session = session();
            $session->setFlashData('error', "Population non trouvée");
            return redirect()->to("/population/index");
        }
    }

    public function delete($id = null)
    {
        $model = new CibleConservationModel();
        $data = $model->find($id);
        if ($data) {
            $nom = $data['nom_cible_conservation'];
            $image = $data['image_cible'];
            $model->delete($id);
            if(file_exists("./".$image) && !is_dir("./".$image)){
                unlink("./".$image);
            }
            $session = session();
			$session->setFlashData('success', "Cible de conservation : '".$nom."' a été supprimée");
            return redirect()->to("/cibleconservation/index");
        } else {
            $session = session();
			$session->setFlashData('error', "Cible de conservation non trouvée");
            return redirect()->to("/cibleconservation/index");
        }
    }
}
