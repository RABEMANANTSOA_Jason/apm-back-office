<?php namespace App\Controllers;

use App\Models\GestionnaireModel;

class Gestionnaire extends BaseController
{

    public function index()
    {
        $data = [];
        $model = new GestionnaireModel();
        $data['gestionnaires'] = $model->findAll();
        return view('gestionnaire/index',$data);
    }

    public function detail($id = NULL){
        $data = [];
        $model = new GestionnaireModel();
        $data['gestionnaire'] = $model->find($id);
        if($data['gestionnaire']){
            return view('gestionnaire/detail',$data);
        }
        else{
            $session = session();
			$session->setFlashData('error', "Gestionnaire non trouvé");
            return redirect()->to("/gestionnaire/index");
        }
    }

    public function create(){
        helper(['form']);
        return view('gestionnaire/add');
    }

    public function add()
    {
        $data = [];
        helper(['form']);
        $model = new GestionnaireModel();  
        if (!$this->validate($model->rules)) {
            $data['validation'] = $this->validator;
        } else {
            $insert = [
                'nom_gestionnaire' => $this->request->getVar('nom_gestionnaire'),
                'description_gestionnaire' => $this->request->getVar('description_gestionnaire'),
                'date_creation_gestionnaire' => $this->request->getVar('date_creation_gestionnaire'),
            ];

            $id = $model->insert($insert);
            $session = session();
            $session->setFlashData('success', 'Gestionnaire ajouté');
            return redirect()->to("/gestionnaire/detail/".$id);
        }
        return view('gestionnaire/add',$data);
    }

    public function edit($id = NULL){
        $data = [];
        helper(['form']);
        $model = new GestionnaireModel();
        $data['gestionnaire'] = $model->find($id);
        if($data['gestionnaire']){
            return view('gestionnaire/edit',$data);
        }
        else{
            $session = session();
			$session->setFlashData('error', "Gestionnaire non trouvé");
            return redirect()->to("/gestionnaire/index");
        }
    }

    public function update($id = null)
    {
        $data = [];
        helper(['form']);
        $model = new GestionnaireModel();
        $data['gestionnaire'] = $model->find($id);
        if($data['gestionnaire']){
            if (!$this->validate($model->update_rules)) {
                $data['validation'] = $this->validator;
                $data['gestionnaire']['id'] = $id;
            } else {

                $update = [
                    'id' => $id,
                    'nom_gestionnaire' => $this->request->getVar('nom_gestionnaire'),
                    'description_gestionnaire' => $this->request->getVar('description_gestionnaire'),
                    'date_creation_gestionnaire' => $this->request->getVar('date_creation_gestionnaire'),
                ];

                $model->save($update);
                $session = session();
                $session->setFlashData('success', 'Gestionnaire modifié');
                return redirect()->to("/gestionnaire/detail/".$id);
            }
            return view('gestionnaire/edit',$data);
        }else{
            $session = session();
            $session->setFlashData('error', 'Gestionnaire non trouvé');
            return redirect()->to("/gestionnaire/index");
        }
    }

    public function delete($id = null)
    {
        $model = new GestionnaireModel();
        $data = $model->find($id);
        if ($data) {
            $gestionnaire = $data['nom_gestionnaire'];
            $model->delete($id);
            $session = session();
			$session->setFlashData('success', "Gestionnaire : '".$gestionnaire."' a été supprimé");
            return redirect()->to("/gestionnaire/index");
        } else {
            $session = session();
			$session->setFlashData('error', "Gestionnaire non trouvé");
            return redirect()->to("/gestionnaire/index");
        }
    }
}
