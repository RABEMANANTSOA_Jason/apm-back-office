<?php

namespace App\Models;

use CodeIgniter\Model;

class RegionModel extends Model
{
    protected $table = 'Regions';
    protected $primaryKey = 'id';
    protected $allowedFields = ['region', 'id_province'];
    protected $rules = [
        'region' => [
            'rules' => 'required|is_unique[regions.region]',
            'errors' => [
                'required' => "Veuillez donner un nom de région",
                'is_unique' => "Cette région existe déjà"
            ]
        ],
        'id_province' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez référencer une province"
            ]
        ]
    ];

    public function getRegionAll(){
        return $this->findAll();
    }
}
