<?= $this->extend('templates/admin_template') ?>

<?= $this->section('content') ?>

<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Aires protégées</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= ('dashboard') ?>">Accueil</a></li>
              <li class="breadcrumb-item active">Aires protégées</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-sm-10">
                    <h3 class="card-title">Tableau des aires protégées</h3>
                  </div>
                  <div class="col-sm-2">
                  <a href="<?= site_url('aire_protegee/create') ?>"><button type="button" class="btn btn-block bg-gradient-success btn-sm">Ajouter une aire protégée</button></a>
                  </div><!-- /.col -->
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Nom</th>
                    <th>Date création</th>
                    <th>Gestionnaire</th>
                    <th>Statut</th>
                    <th>Mode de gestion</th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php foreach ($listeAP as $item):?>
                    <tr>
                      <td><?= $item->nom_du_site ?></td>
                      <td><?= $item->date_creation_ap ?></td>
                      <td><?= $item->nom_gestionnaire ?></td>
                      <td><?= $item->statut_fr ?></td>
                      <td><?= $item->type_mode_gestion ?></td>
                      <td><a href="<?= site_url('aire_protegee/detail/') ?><?= $item->id_ap?>">Voir</a> | <a href="<?= site_url('aire_protegee/edit/') ?><?= $item->id_ap?>"">Modifier</a></td>
                    </tr>
                  <?php endforeach;?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

<?= $this->endSection('') ?>