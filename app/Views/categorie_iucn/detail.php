<?= $this->extend('templates/admin_template') ?>

<?= $this->section('content') ?>

<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Catégorie IUCN détail</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= site_url('/categorie_iucn') ?>">Accueil</a></li>
              <li class="breadcrumb-item active">Détail</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
        <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-sm-10">
            <h3 class="card-title">
                <i class="fas fa-newspaper"></i>
                <?= $categorie_iucn['statut_fr'] ?>
            </h3>
            </div>
            <div class="col-sm-2">
            <a href="<?= site_url('categorie_iucn/edit/') ?><?= $categorie_iucn['id']?>"><button type="button" class="btn btn-block bg-gradient-success btn-sm">Modifier</button></a>
            </div>
          </div>
          </div>
        <!-- /.card-header -->
        <div class="card-body">
            <blockquote>
                <b>Catégorie IUCN</b>
                <p><?= $categorie_iucn['categorie']?></p>
                <b>Code</b>
                <p><?= $categorie_iucn['code_statut_categorie_iucn']?></p>
                <b>Statut(FR)</b>
                <p><?= $categorie_iucn['statut_fr']?></p>
                <b>Statut(MG)</b>
                <p><?= $categorie_iucn['statut_mg']?></p>
                <b>Description</b>
                <p><?= $categorie_iucn['description_categorie_iucn']?></p>
            </blockquote>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
      </div><!-- /.container-fluid -->
    </div>

<?= $this->endSection('') ?>