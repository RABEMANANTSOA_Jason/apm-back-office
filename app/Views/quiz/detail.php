<?= $this->extend('templates/admin_template') ?>

<?= $this->section('content') ?>

<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Quiz</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?= site_url('visitevirtuelle') ?>">Accueil</a></li>
          <li class="breadcrumb-item active">Quiz détail</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-primary card-outline">
          <div class="card-header">
            <div class="row">
              <div class="col-sm-10">
                <h3 class="card-title">
                  <i class="fas fa-gamepad"></i>
                  <?= $quiz[0]->nom_quiz ?>
                </h3>
              </div>
              <div class="col-sm-2">
                <a href="<?= site_url('quiz/edit/') ?>"><button type="button" class="btn btn-block bg-gradient-danger btn-sm">Supprimer</button></a>
              </div><!-- /.col -->
            </div>
          </div>
          <div class="card-body pad table-responsive">
            <h4>Questions</h4>
            <div class="col-12">
              <div class="card card-primary card-outline card-outline-tabs">
                <div class="card-header p-0 border-bottom-0">
                  <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                    <?php $i = 1; ?>
                    <?php $j = 1; ?>
                    <?php foreach ($quiz as $item) : ?>
                      <?php if ($i === 1) : ?>
                        <li class="nav-item">
                          <a class="nav-link active" id="question-<?= $item->id ?>-tab" data-toggle="pill" href="#question-<?= $item->id ?>" role="tab" aria-controls="custom-tabs-four-home" aria-selected="true"><?= $j ?></a>
                        </li>
                        <?php $i++;
                        $j++; ?>
                      <?php else : ?>
                        <li class="nav-item">
                          <a class="nav-link" id="question-<?= $item->id ?>-tab" data-toggle="pill" href="#question-<?= $item->id ?>" role="tab" aria-controls="custom-tabs-four-home" aria-selected="true"><?= $j ?></a>
                        </li>
                        <?php $i++;
                        $j++; ?>
                      <?php endif; ?>
                    <?php endforeach; ?>
                  </ul>
                </div>
                <div class="card-body">
                  <div class="tab-content" id="custom-tabs-four-tabContent">
                    <?php $i = 1; ?>
                    <?php foreach ($quiz as $item) : ?>
                      <?php if ($i === 1) : ?>
                        <div class="tab-pane text-left fade show active" id="question-<?= $item->id ?>" role="tabpanel" aria-labelledby="question-<?= $item->id ?>-tab">
                          <p>Question : <?= $item->question ?></p>
                          <ul>
                            <li>Option 1 : <?= $item->optionA ?></li>
                            <li>Option 2 : <?= $item->optionB ?></li>
                            <li>Option 3 : <?= $item->optionC ?></li>
                            <li>Option 4 : <?= $item->optionD ?></li>
                          </ul>
                          <br />
                          <p>Réponse : <?= $item->reponse ?></p>
                        </div>
                        <?php $i++; ?>
                      <?php else : ?>
                        <div class="tab-pane fade" id="question-<?= $item->id ?>" role="tabpanel" aria-labelledby="question-<?= $item->id ?>-tab">
                          <p>Question : <?= $item->question ?></p>
                          <ul>
                            <li>Option 1 : <?= $item->optionA ?></li>
                            <li>Option 2 : <?= $item->optionB ?></li>
                            <li>Option 3 : <?= $item->optionC ?></li>
                            <li>Option 4 : <?= $item->optionD ?></li>
                          </ul>
                          <br />
                          <p>Réponse : <?= $item->reponse ?></p>
                        </div>
                        <?php $i++; ?>
                      <?php endif; ?>
                    <?php endforeach; ?>
                  </div>
                </div>
                <!-- /.card -->
              </div>
            </div>
            <?php if (isset($validation)) : ?>
                <div class="row">
                    <div class="col-12">
                        <div class="card card-danger direct-chat direct-chat-danger shadow-lg">
                            <div class="card-header">
                                <h3 class="card-title">Erreur(s)</h3>
                            </div>
                            <div class="card-body">
                                <?= $validation->listErrors() ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <form action="<?= site_url('quiz/addQuestion/') ?><?= $quiz[0]->id_quiz?>" method="post" enctype='multipart/form-data'>

              <div class="col-md-12">
                <div class="card card-info">
                  <div class="card-header">
                    <h3 class="card-title">Ajouter une question</h3>
                    <div class="card-tools">
                      <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group">
                          <label>Question</label>
                          <input type="text" class="form-control" name="question" placeholder="Première question" value="<?= set_value('question') ?>">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label>Choix 1</label>
                  <input type="text" class="form-control" name="optionA" placeholder="Choix 1" value="<?= set_value('optionA') ?>">
                </div>
                <div class="form-group">
                  <label>Choix 2</label>
                  <input type="text" class="form-control" name="optionB" placeholder="Choix 2" value="<?= set_value('optionB') ?>">
                </div>
                <div class="form-group">
                  <label>Choix 3</label>
                  <input type="text" class="form-control" name="optionC" placeholder="Choix 3" value="<?= set_value('optionC') ?>">
                </div>
                <div class="form-group">
                  <label>Choix 4</label>
                  <input type="text" class="form-control" name="optionD" placeholder="Choix 4" value="<?= set_value('optionD') ?>">
                </div>
                <div class="row">
                  <div class="col-12"><label>Réponse</label></div>
                  <div class="col-2"></div>
                  <div class="col-2">
                    <div class="custom-control custom-radio">
                      <input class="custom-control-input" type="radio" id="customRadio1" name="reponse" value="1" checked>
                      <label for="customRadio1" class="custom-control-label">Choix 1</label>
                    </div>
                  </div>
                  <div class="col-2">
                    <div class="custom-control custom-radio">
                      <input class="custom-control-input" type="radio" id="customRadio2" name="reponse" value="2">
                      <label for="customRadio2" class="custom-control-label">Choix 2</label>
                    </div>
                  </div>
                  <div class="col-2">
                    <div class="custom-control custom-radio">
                      <input class="custom-control-input" type="radio" id="customRadio3" name="reponse" value="3">
                      <label for="customRadio3" class="custom-control-label">Choix 3</label>
                    </div>
                  </div>
                  <div class="col-2">
                    <div class="custom-control custom-radio">
                      <input class="custom-control-input" type="radio" id="customRadio4" name="reponse" value="4">
                      <label for="customRadio4" class="custom-control-label">Choix 4</label>
                    </div>
                  </div>
                  <div class="col-2"></div>
                </div>
              </div>
              <br />
              <div class="col-md-12">
                <button type="submit" class="btn btn-block btn-success btn-lg">Enregsitrer</button>
              </div>
            </form>
          </div>

        </div>
        <!-- /.card -->
      </div>
</section>

<?= $this->endSection('') ?>