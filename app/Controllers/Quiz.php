<?php namespace App\Controllers;

use App\Models\ActualitesModel;
use App\Models\QuizModel;
use App\Models\QuizDetailModel;
use App\Models\custom\QuizCustomModel;

class Quiz extends BaseController
{

    public function index()
    {
        $data = [];
        helper(['form']);
        $model = new QuizModel();
        $data['quiz'] = $model->findAll();
        /*echo '<pre>';
            print_r($data['actualites']);
        echo '<pre>';*/
        return view('quiz/index',$data);
    }

    public function detail($id = NULL){
        $data = [];
        helper(['form']);
        $db = db_connect();
        $model = new QuizCustomModel($db);
        $data['quiz'] = $model->getDetailQuiz($id);
        if($data['quiz']){
            /*echo '<pre>';
            print_r($data['aire_protegee']);
            echo '<pre>';*/
            return view('quiz/detail',$data);
        }
        else{
            $session = session();
			$session->setFlashData('error', "Quiz non trouvé");
            return redirect()->to("/quiz/index");
        }
    }

    public function create(){
        $data = [];
        helper(['form']);
        return view('quiz/add');
    }

    public function add()
    {
        $data = [];
        helper(['form']);
        $model = new QuizDetailModel();
        if (!$this->validate($model->rules)) {
            $data['validation'] = $this->validator;
        } else {
            $insertQuiz = [
                'nom_quiz' => $this->request->getVar('nom_quiz'),
            ];
            $insertDetail = [
                'question' => $this->request->getVar('question'),
                'optionA' => $this->request->getVar('optionA'),
                'optionB' => $this->request->getVar('optionB'),
                'optionC' => $this->request->getVar('optionC'),
                'optionD' => $this->request->getVar('optionD'),
                'reponse' => $this->request->getVar('reponse'),
            ];
            $db = db_connect();
            $custom = new QuizCustomModel($db);
            $id = $custom->insertQuiz($insertQuiz,$insertDetail);
            $session = session();
            $session->setFlashData('success', 'Quiz : '.$this->request->getVar('nom_quiz').' créé');
            return redirect()->to("/quiz/detail/".$id);
        }
        return view('quiz/add',$data);
    }

    public function addQuestion($id = NULL)
    {
        $data = [];
        helper(['form']);
        $model = new QuizDetailModel();
        $db = db_connect();
        $custom = new QuizCustomModel($db);
        $data['quiz'] = $custom->getDetailQuiz($id);
        if (!$this->validate($model->rules_question)) {
            $data['validation'] = $this->validator;
        } else {
            $insertDetail = [
                'question' => $this->request->getVar('question'),
                'optionA' => $this->request->getVar('optionA'),
                'optionB' => $this->request->getVar('optionB'),
                'optionC' => $this->request->getVar('optionC'),
                'optionD' => $this->request->getVar('optionD'),
                'reponse' => $this->request->getVar('reponse'),
                'id_quiz' => $id,
            ];
            $custom->insertQestion($insertDetail);
            $session = session();
            $session->setFlashData('success', 'Question ajoutée');
            return redirect()->to("/quiz/detail/".$id);
        }
        return view('quiz/detail',$data);
    }

    public function edit($id = NULL){
        $data = [];
        helper(['form']);
        $model = new ActualitesModel();
        $data['actualite'] = $model->find($id);
        if($data['actualite']){
            return view('actualites/edit',$data);
        }
        else{
            $session = session();
			$session->setFlashData('error', "Actualité non trouvée");
            return redirect()->to("/actualites/index");
        }
    }

    public function update($id = null)
    {
        $data = [];
        helper(['form']);
        $model = new ActualitesModel();
        $data['actualite'] = $model->find($id);
        if($data['actualite']){
            if (!$this->validate($model->rules)) {
                $data['validation'] = $this->validator;
                $data['actualite']['id'] = $id;
            } else {
                $image = $data['actualite']['image_actualite'];
                $file = $this->request->getFile('image_actualite');

                $update = [
                    'id' => $id,
                    'titre_actualite' => $this->request->getVar('titre_actualite'),
                    'contenu_actualite' => $this->request->getVar('contenu_actualite'),
                    'auteur_actualite' => $this->request->getVar('auteur_actualite'),
                ];

                if($file->isValid() && !$file->hasMoved()){
                    $file->move($this->relativepath);
                    if(!file_exists("./".$image)){
                        unlink("./".$image);
                    }
                    $update['image_actualite'] = $this->path.$file->getName();;
                }
                $model->save($update);
                $session = session();
                $session->setFlashData('success', 'Actualité modifiée');
                return redirect()->to("/actualites/detail/".$id);
            }
            return view('actualites/edit',$data);
        }else{
            $session = session();
            $session->setFlashData('error', 'Actualité non trouvée');
            return redirect()->to("/actualites/index");
        }
    }

    public function delete($id = null)
    {
        $model = new ActualitesModel();
        $data = $model->find($id);
        if ($data) {
            $titre = $data['titre_actualite'];
            $image = $data['image_actualite'];
            $model->delete($id);
            if(file_exists("./".$image)){
                unlink("./".$image);
            }
            $session = session();
			$session->setFlashData('success', "Actualité : '".$titre."' a été supprimée");
            return redirect()->to("/actualites/index");
        } else {
            $session = session();
			$session->setFlashData('error', "Actualité non trouvée");
            return redirect()->to("/actualites/index");
        }
    }
}
