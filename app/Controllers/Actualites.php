<?php namespace App\Controllers;

use App\Models\ActualitesModel;

class Actualites extends BaseController
{
    private $path = '/images/actus/';
    private $relativepath = './images/actus';

    public function index()
    {
        $data = [];
        helper(['form']);
        $model = new ActualitesModel();
        $data['actualites'] = $model->getActualites();
        /*echo '<pre>';
            print_r($data['actualites']);
        echo '<pre>';*/
        return view('actualites/index',$data);
    }

    public function detail($id = NULL){
        $data = [];
        $model = new ActualitesModel();
        $data['actualite'] = $model->find($id);
        if($data['actualite']){
            return view('actualites/detail',$data);
        }
        else{
            $session = session();
			$session->setFlashData('error', "Actualité non trouvée");
            return redirect()->to("/actualites/index");
        }
    }

    public function add()
    {
        $data = [];
        helper(['form']);
        $model = new ActualitesModel();
        $data['actualites'] = $model->getActualites();
        
        if (!$this->validate($model->rules)) {
            $data['validation'] = $this->validator;
        } else {
            $file = $this->request->getFile('image_actualite');
            $insert = [
                'titre_actualite' => $this->request->getVar('titre_actualite'),
                'contenu_actualite' => $this->request->getVar('contenu_actualite'),
                'auteur_actualite' => $this->request->getVar('auteur_actualite'),
            ];

            if($file->isValid() && !$file->hasMoved()){
                $file->move($this->relativepath);
                $insert['image_actualite'] = $this->path.$file->getName();
            }
            $id = $model->insert($insert);
            $session = session();
            $session->setFlashData('success', 'Actualité publiée');
            return redirect()->to("/actualites/index");
        }
        return view('actualites/index',$data);
    }

    public function edit($id = NULL){
        $data = [];
        helper(['form']);
        $model = new ActualitesModel();
        $data['actualite'] = $model->find($id);
        if($data['actualite']){
            return view('actualites/edit',$data);
        }
        else{
            $session = session();
			$session->setFlashData('error', "Actualité non trouvée");
            return redirect()->to("/actualites/index");
        }
    }

    public function update($id = null)
    {
        $data = [];
        helper(['form']);
        $model = new ActualitesModel();
        $data['actualite'] = $model->find($id);
        if($data['actualite']){
            if (!$this->validate($model->rules)) {
                $data['validation'] = $this->validator;
                $data['actualite']['id'] = $id;
            } else {
                $image = $data['actualite']['image_actualite'];
                $file = $this->request->getFile('image_actualite');

                $update = [
                    'id' => $id,
                    'titre_actualite' => $this->request->getVar('titre_actualite'),
                    'contenu_actualite' => $this->request->getVar('contenu_actualite'),
                    'auteur_actualite' => $this->request->getVar('auteur_actualite'),
                ];

                if($file->isValid() && !$file->hasMoved()){
                    $file->move($this->relativepath);
                    if(file_exists("./".$image) && !is_dir("./".$image)){
                        unlink("./".$image);
                    }
                    $update['image_actualite'] = $this->path.$file->getName();;
                }
                $model->save($update);
                $session = session();
                $session->setFlashData('success', 'Actualité modifiée');
                return redirect()->to("/actualites/detail/".$id);
            }
            return view('actualites/edit',$data);
        }else{
            $session = session();
            $session->setFlashData('error', 'Actualité non trouvée');
            return redirect()->to("/actualites/index");
        }
    }

    public function delete($id = null)
    {
        $model = new ActualitesModel();
        $data = $model->find($id);
        if ($data) {
            $titre = $data['titre_actualite'];
            $image = $data['image_actualite'];
            $model->delete($id);
            if(file_exists("./".$image) && !is_dir("./".$image)){
                unlink("./".$image);
            }
            $session = session();
			$session->setFlashData('success', "Actualité : '".$titre."' a été supprimée");
            return redirect()->to("/actualites/index");
        } else {
            $session = session();
			$session->setFlashData('error', "Actualité non trouvée");
            return redirect()->to("/actualites/index");
        }
    }
}
