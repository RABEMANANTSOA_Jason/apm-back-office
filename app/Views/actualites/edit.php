<?= $this->extend('templates/admin_template') ?>

<?= $this->section('content') ?>

<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Actualités</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= ('dashboard') ?>">Accueil</a></li>
              <li class="breadcrumb-item active">Actualités</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Timelime example  -->
        <div class="row">
          <div class="col-md-12">
            <!-- The time line -->
            <div class="timeline">
                <!-- timeline time label -->
                <div class="time-label">
                    <span class="bg-blue">Rédiger un article</span>
                </div>
                <div>
                <i class="fas fa-plus bg-green"></i>
                    <div class="timeline-item">
                    <?php if (isset($validation)) : ?>
                      <div class="row">
                        <div class="col-12">
                          <div class="card card-danger direct-chat direct-chat-danger shadow-lg">
                            <div class="card-header">
                              <h3 class="card-title">Erreur(s)</h3>
                            </div>
                            <div class="card-body">
                              <?= $validation->listErrors() ?>
                            </div>
                          </div>
                        </div>
                      </div>
                    <?php endif; ?>
                    <form action="<?= site_url('actualites/update/')?><?= $actualite['id']?>" method="post" enctype="multipart/form-data">

                      <h3 class="timeline-header">
                        <input type="text" class="form-control" name="titre_actualite" placeholder="Titre de l'article" value="<?= $actualite['titre_actualite']?>">
                      </h3>
                      <div class="timeline-body">
                        <textarea class="form-control" name="contenu_actualite" rows="20" placeholder="Rédiger ici"><?= $actualite['contenu_actualite'] ?></textarea>
                        <div class="custom-file">
                          <input type="file" class="custom-file-input" id="image_actualite" name="image_actualite" onchange="readURL(this);">
                          <label class="custom-file-label" for="exampleInputFile">Importer une image</label>
                        </div>
                        <img id="blah" class="img-fluid" src="#" alt="Votre image" />
                      </div>
                      <br/>
                      <div class="timeline-footer">
                        <input type="text" class="form-control" name="auteur_actualite" placeholder="Auteur" value="<?= $actualite['auteur_actualite']?>">
                        <br/>
                        <button type="submit" class="btn btn-success btn-sm">Publier</button>
                      </div>
                      </form> 
                    </div>
                  </div>
              <div>
                <i class="fas fa-clock bg-gray"></i>
              </div>
            </div>
          </div>
          <!-- /.col -->
        </div>
      </div>
      <!-- /.timeline -->

    </section>
    <!-- /.content -->
    <script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    </script>
<?= $this->endSection('') ?>