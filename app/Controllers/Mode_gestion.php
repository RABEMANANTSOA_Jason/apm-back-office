<?php namespace App\Controllers;

use App\Models\Mode_GestionModel;

class Mode_gestion extends BaseController
{

    public function index()
    {
        $data = [];
        $model = new Mode_GestionModel();
        $data['mode_gestion'] = $model->findAll();
        return view('mode_gestion/index',$data);
    }

    public function detail($id = NULL){
        $data = [];
        $model = new Mode_GestionModel();
        $data['mode_gestion'] = $model->find($id);
        if($data['mode_gestion']){
            return view('mode_gestion/detail',$data);
        }
        else{
            $session = session();
			$session->setFlashData('error', "Mode de gestion non trouvé");
            return redirect()->to("/mode_gestion/index");
        }
    }

    public function create(){
        helper(['form']);
        return view('mode_gestion/add');
    }

    public function add()
    {
        $data = [];
        helper(['form']);
        $model = new Mode_GestionModel();        
        if (!$this->validate($model->rules)) {
            $data['validation'] = $this->validator;
        } else {
            $insert = [
                'type_mode_gestion' => $this->request->getVar('type_mode_gestion'),
                'description' => $this->request->getVar('description'),
            ];

            $id = $model->insert($insert);
            $session = session();
            $session->setFlashData('success', 'Mode de gestion ajouté');
            return redirect()->to("/mode_gestion/detail/".$id);
        }
        return view('mode_gestion/add',$data);
    }

    public function edit($id = NULL){
        $data = [];
        helper(['form']);
        $model = new Mode_GestionModel();
        $data['mode_gestion'] = $model->find($id);
        if($data['mode_gestion']){
            return view('mode_gestion/edit',$data);
        }
        else{
            $session = session();
			$session->setFlashData('error', "Mode de gestion non trouvé");
            return redirect()->to("/mode_gestion/index");
        }
    }

    public function update($id = null)
    {
        $data = [];
        helper(['form']);
        $model = new Mode_GestionModel();
        $data['mode_gestion'] = $model->find($id);
        if($data['mode_gestion']){
            if (!$this->validate($model->update_rules)) {
                $data['validation'] = $this->validator;
                $data['mode_gestion']['id'] = $id;
            } else {

                $update = [
                    'id' => $id,
                    'type_mode_gestion' => $this->request->getVar('type_mode_gestion'),
                    'description' => $this->request->getVar('description'),
                ];

                $model->save($update);
                $session = session();
                $session->setFlashData('success', 'Mode de gestion modifié');
                return redirect()->to("/mode_gestion/detail/".$id);
            }
            return view('mode_gestion/edit',$data);
        }else{
            $session = session();
            $session->setFlashData('error', 'Mode de gestion non trouvé');
            return redirect()->to("/mode_gestion/index");
        }
    }

    public function delete($id = null)
    {
        $model = new Mode_GestionModel();
        $data = $model->find($id);
        if ($data) {
            $mode_gestion = $data['type_mode_gestion'];
            $model->delete($id);
            $session = session();
			$session->setFlashData('success', "Mode de gestion : '".$mode_gestion."' a été supprimé");
            return redirect()->to("/mode_gestion/index");
        } else {
            $session = session();
			$session->setFlashData('error', "Mode de gestion non trouvé");
            return redirect()->to("/mode_gestion/index");
        }
    }
}
