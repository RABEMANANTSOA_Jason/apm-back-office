<?= $this->extend('templates/admin_template') ?>

<?= $this->section('content') ?>

<!-- Content Header (Page header) -->
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Visites virtuelles</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= site_url('visite_virtuelle') ?>">Accueil</a></li>
              <li class="breadcrumb-item active">Visionnage</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                <div class="card card-primary card-outline">
              <div class="card-header">
                <div class="row">
                  <div class="col-sm-10">
                  <h3 class="card-title">
                  <i class="fas fa-vr-cardboard"></i>
                    <?= $visite[0]->nom_visite?>
                </h3>
                  </div>
                  <div class="col-sm-2">
                  <a href="<?= site_url('visite_virtuelle/edit/') ?><?= $visite[0]->id?>"><button type="button" class="btn btn-block bg-gradient-success btn-sm">Modifier</button></a>
                  </div><!-- /.col -->
                </div>
              </div>
              <div class="card-body pad table-responsive">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-1"></div>
                            <div class="col-2">
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="customRadio1" name="customRadio" value="<?= $visite[0]->image_nord?>" onclick="navigate(this);" <?php if($visite[0]->image_nord == null):?>disabled <?php endif;?>>
                                    <label for="customRadio1" class="custom-control-label">Nord</label>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="customRadio2" name="customRadio" value="<?= $visite[0]->image_ouest?>" onclick="navigate(this);" <?php if($visite[0]->image_ouest == null):?>disabled <?php endif;?>>
                                    <label for="customRadio2" class="custom-control-label">Ouest</label>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="customRadio3" name="customRadio" value="<?= $visite[0]->image_centre?>" onclick="navigate(this);" checked>
                                    <label for="customRadio3" class="custom-control-label">Centre</label>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="customRadio4" name="customRadio" value="<?= $visite[0]->image_est?>" onclick="navigate(this);" <?php if($visite[0]->image_est == null):?>disabled <?php endif;?>>
                                    <label for="customRadio4" class="custom-control-label">Est</label>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="customRadio5" name="customRadio" value="<?= $visite[0]->image_sud?>" onclick="navigate(this);" <?php if($visite[0]->image_sud == null):?>disabled <?php endif;?>>
                                    <label for="customRadio5" class="custom-control-label">Sud</label>
                                </div>
                            </div>
                            <div class="col-1"></div>

                        </div>
                    </div>
              </div>
              <!-- /.card -->
            </div>
                <div id="viewer"></div>
                </div>
            </div>
        </div>
    </section>

<!-- Image 360° viewer -->
<script src="https://cdn.jsdelivr.net/npm/three/build/three.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/uevent@2/browser.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/photo-sphere-viewer@4/dist/photo-sphere-viewer.min.js"></script>

<style>
  /* the viewer container must have a defined size */
  #viewer {
    width: auto;
    height: 70vh;
  }
</style>

<script type="text/javascript">
  var viewer = new PhotoSphereViewer.Viewer({
    container: document.querySelector('#viewer'),
    panorama: '<?= base_url()?><?= $visite[0]->image_centre?>'
  })

  function navigate(input){
    var base = '<?= base_url()?>';
    var image = $("input[name='customRadio']:checked").val();
    viewer.setPanorama(base+image);
    console.log(base.concat(image));
  }
</script>

<?= $this->endSection('') ?>