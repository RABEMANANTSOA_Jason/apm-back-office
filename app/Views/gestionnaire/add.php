<?= $this->extend('templates/admin_template') ?>

<?= $this->section('content') ?>

<!-- Content Header (Page header) -->
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Gestionnaire</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= site_url('/gestionnaire') ?>">Accueil</a></li>
              <li class="breadcrumb-item active">Ajout</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Nouveau gestionnaire</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
              </button>
            </div>
          </div>
          <?php if (isset($validation)) : ?>
          <div class="row">
            <div class="col-12">
              <div class="card card-danger direct-chat direct-chat-danger shadow-lg">
                <div class="card-header">
                  <h3 class="card-title">Erreur(s)</h3>
                </div>
                <div class="card-body">
                  <?= $validation->listErrors() ?>
                </div>
              </div>
            </div>
          </div>
        <?php endif; ?>
          <!-- /.card-header -->
          <div class="card-body">
            <!-- /.col (left) -->
            <form action="<?= site_url('gestionnaire/add') ?>" method="post">

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Nom du gestionnaire</label>
                        <input type="text" class="form-control" name="nom_gestionnaire" placeholder="Gestionnaire" value="<?= set_value('nom_gestionnaire') ?>">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Date de création</label>
                        <div class="input-group">                                  
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                            </div>
                        <input type="text" class="form-control" name="date_creation_gestionnaire" data-inputmask-alias="datetime" data-inputmask-inputformat="yyyy/mm/dd" data-mask value="<?= set_value('date_creation_gestionnaire') ?>">
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="card card-outline card-info">
                    <div class="card-header">
                        <h3 class="card-title">
                            Description
                        </h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body pad">
                        <div class="mb-3">
                            <textarea class="textarea" placeholder="Place some text here" name="description_gestionnaire"
                            style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" ><?= set_value('description_gestionnaire') ?></textarea>
                        </div>
                </div>
            </div>
            </div>
            <div class="col-md-12">
                <button type="submit" class="btn btn-block btn-success btn-lg">Enregistrer</button>
            </div>
            </form>
        </div>
        <!-- /.card -->

        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    

<?= $this->endSection('') ?>