<?php

namespace App\Models;

use CodeIgniter\Model;

class ProvinceModel extends Model
{
    protected $table = 'Provinces';
    protected $primaryKey = 'id';
    protected $allowedFields = ['province'];
    protected $rules = [
        'province' => [
            'rules' => 'required|is_unique[provinces.province]',
            'errors' => [
                'required' => "Veuillez donner un nom de province",
                'is_unique' => "Cette province existe déjà"
            ]
        ]
    ];

    public function getProvinceAll(){
        return $this->findAll();
    }
}
