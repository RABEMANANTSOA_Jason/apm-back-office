<?= $this->extend('templates/admin_template') ?>

<?= $this->section('content') ?>

<!-- Content Header (Page header) -->
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Aires protégées</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= site_url('/aire_protegee') ?>">Accueil</a></li>
              <li class="breadcrumb-item active">Modification</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Modification aire protégée</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
              </button>
            </div>
          </div>
          <?php if (isset($validation)) : ?>
          <div class="row">
            <div class="col-12">
              <div class="card card-danger direct-chat direct-chat-danger shadow-lg">
                <div class="card-header">
                  <h3 class="card-title">Erreur(s)</h3>
                </div>
                <div class="card-body">
                  <?= $validation->listErrors() ?>
                </div>
              </div>
            </div>
          </div>
        <?php endif; ?>
          <!-- /.card-header -->
          <div class="card-body">
            <!-- /.col (left) -->
            <form action="<?= site_url('aire_protegee/update/') ?><?= $ap['id_ap']?>" method="post" enctype='multipart/form-data'>

          <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Informations générales</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Nom du site</label>
                                <input type="text" class="form-control" name="nom_du_site" placeholder="Nom du site" value="<?= $ap['nom_du_site'] ?>">
                            </div>
                            <div class="form-group">
                                <label>Date de création</label>
                                <div class="input-group">                                  
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                    </div>
                                    <input type="text" class="form-control" name="date_creation_ap" data-inputmask-alias="datetime" data-inputmask-inputformat="yyyy/mm/dd" data-mask value="<?= $ap['date_creation_ap'] ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>N°Arrété de mise en protection temporaire</label>
                                <input type="text" class="form-control" name="arrete_mise_en_protection" placeholder="N°Arrété de mise en protection temporaire" value="<?= $ap['arrete_mise_en_protection'] ?>">
                            </div>
                            <div class="form-group">
                                <label>Arrêté/Décret en cours</label>
                                <input type="text" class="form-control" name="arrete_decret_en_cours" placeholder="Arrêté/Décret en cours" value="<?= $ap['arrete_decret_en_cours'] ?>">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Type de l'aire protégée</label>
                                <input type="text" class="form-control" name="type_ap" placeholder="Terrestre, Marine, Terrestre/Marine, ..." value="<?= $ap['type_ap'] ?>">
                            </div>
                            <div class="form-group">
                                <label>Date de signature</label>
                                <div class="input-group">                                  
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                    </div>
                                    <input type="text" class="form-control" name="date_signature" data-inputmask-alias="datetime" data-inputmask-inputformat="yyyy/mm/dd" data-mask value="<?= $ap['date_signature'] ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Date de mise en protection temporaire</label>
                                <div class="input-group">                                  
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                    </div>
                                    <input type="text" class="form-control" name="date_mise_en_protection" data-inputmask-alias="datetime" data-inputmask-inputformat="yyyy/mm/dd" data-mask value="<?= $ap['date_mise_en_protection'] ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>N°décret</label>
                                <input type="text" class="form-control" name="numero_decret" placeholder="N°décret" value="<?= $ap['numero_decret'] ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Statut et catégorie IUCN</label>
                                <select class="form-control select2bs4" name="id_statut_categorie_iucn" style="width: 100%;">
                                    <?php foreach ($statuts as $item):?>
                                        <?php if($item['id'] == $ap['id_statut_categorie_iucn']): ?>
                                            <option value="<?= $item['id'] ?>" <?= set_select('id_statut_categorie_iucn', $item['id']); ?> selected><?= $item['categorie'] ?>-<?= $item['statut_fr'] ?>(<?= $item['code_statut_categorie_iucn'] ?>)</option>
                                        <?php else:?>
                                            <option value="<?= $item['id'] ?>" <?= set_select('id_statut_categorie_iucn', $item['id']); ?>><?= $item['categorie'] ?>-<?= $item['statut_fr'] ?>(<?= $item['code_statut_categorie_iucn'] ?>)</option>
                                        <?php endif;?>
                                    <?php endforeach;?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Gestionnaire</label>
                                <select class="form-control select2bs4" name="id_gestionnaire" style="width: 100%;">
                                    <?php foreach ($gestionnaires as $item):?>
                                        <?php if($item['id'] == $ap['id_gestionnaire']): ?>
                                            <option value="<?= $item['id'] ?>" <?= set_select('id_gestionnaire', $item['id']); ?> selected><?= $item['nom_gestionnaire']?></option>                                            
                                        <?php else:?>
                                            <option value="<?= $item['id'] ?>" <?= set_select('id_gestionnaire', $item['id']); ?>><?= $item['nom_gestionnaire']?></option>                                            
                                        <?php endif;?>
                                    <?php endforeach;?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Mode de gestion</label>
                                <select class="form-control select2bs4" name="id_mode_gestion" style="width: 100%;">
                                    <?php foreach ($mode_gestion as $item):?>
                                        <?php if($item['id'] == $ap['id_mode_gestion']): ?>
                                            <option value="<?= $item['id'] ?>" <?= set_select('id_mode_gestion', $item['id']); ?> selected><?= $item['type_mode_gestion'] ?></option>
                                        <?php else:?>
                                            <option value="<?= $item['id'] ?>" <?= set_select('id_mode_gestion', $item['id']); ?>><?= $item['type_mode_gestion'] ?></option>
                                        <?php endif;?>
                                    <?php endforeach;?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="card card-outline card-info">
                                <div class="card-header">
                                    <h3 class="card-title">
                                        Historique
                                    </h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body pad">
                                    <div class="mb-3">
                                        <textarea class="textarea" placeholder="Place some text here" name="historique"
                                        style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" ><?= $ap['historique'] ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card -->

            <div class="card card-success">
                <div class="card-header">
                    <h3 class="card-title">Informations géographiques</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Régions</label>
                                <div class="select2-blue">
                                    <select class="select2" name="regions[]" multiple="multiple" data-placeholder="Selectionner une ou plusieurs régions" style="width: 100%;">
                                        <?php foreach($regions as $item):?>
                                            <?php foreach($ap['regions'] as $region):?>
                                                <?php if($item['region'] == $region): ?>
                                                    <option value="<?= $item['region'] ?>" <?= set_select('region', $item['region']); ?> selected><?= $item['region'] ?></option>
                                                <?php else:?>
                                                    <option value="<?= $item['region'] ?>" <?= set_select('region', $item['region']); ?>><?= $item['region'] ?></option>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Districts</label>
                                <div class="select2-purple">
                                    <select class="select2" name="districts[]" multiple="multiple" data-placeholder="Selectionner une ou plusieurs districts" style="width: 100%;">
                                        <?php foreach ($districts as $item):?>
                                            <?php foreach($ap['districts'] as $district):?>
                                                <?php if($item['district'] == $district): ?>
                                                    <option value="<?= $item['district'] ?>" <?= set_select('districts', $item['district']); ?> selected><?= $item['district'] ?></option>
                                                <?php else:?>
                                                    <option value="<?= $item['district'] ?>" <?= set_select('districts', $item['district']); ?>><?= $item['district'] ?></option>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Communes</label>
                                <div class="select2-purple">
                                    <select class="select2" name="communes[]" multiple="multiple" data-placeholder="Selectionner une ou plusieurs communes" style="width: 100%;">
                                        <?php foreach ($communes as $item):?>
                                            <?php foreach($ap['communes'] as $commune):?>
                                                <?php if($item['commune'] == $commune): ?>
                                                    <option value="<?= $item['commune'] ?>" <?= set_select('communes', $item['commune']); ?> selected><?= $item['commune'] ?></option>
                                                <?php else:?>
                                                    <option value="<?= $item['commune'] ?>" <?= set_select('communes', $item['commune']); ?>><?= $item['commune'] ?></option>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Fokontony</label>
                                <div class="select2-purple">
                                    <select class="select2" name="fokontany[]" multiple="multiple" data-placeholder="Selectionner un ou plusieurs fokontany" style="width: 100%;">
                                        <?php foreach ($fokontany as $item):?>
                                            <?php foreach($ap['fokontany'] as $fokontany):?>
                                                <?php if($item['fokontany'] == $fokontany): ?>
                                                    <option value="<?= $item['fokontany'] ?>" <?= set_select('fokontany', $item['fokontany']); ?> selected><?= $item['fokontany'] ?></option>
                                                <?php else:?>
                                                    <option value="<?= $item['fokontany'] ?>" <?= set_select('fokontany', $item['fokontany']); ?>><?= $item['fokontany'] ?></option>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Superficie Dossier suivant le decret (Ha)</label>
                                <input type="number" min="0" class="form-control" name="superficie_decret" placeholder="0" value="<?= $ap['superficie_decret'] ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Superficie ND (Ha)</label>
                                <input type="number" min="0" class="form-control" name="superficie_nd" placeholder="0" value="<?= $ap['superficie_nd'] ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Superficie ZT</label>
                                <input type="number" min="0" class="form-control" name="superficie_zt" placeholder="0" value="<?= $ap['superficie_zt'] ?>">
                            </div>
                        </div>
                        <input type="hidden" name="id_geolocalisation" value="<?= $geolocalisation[0]->id?>"?>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Données géographiques</label>
                                <textarea class="form-control" name="geojson" rows="3" placeholder="{ type : Feature, properties: { Id: 0, NOM_AP_: Zombitse Vohibasia, DATAADMIN: MNP, hectares: 80672.113 }, geometry: { type: MultiPolygon, coordinates: [ [ [ [ 44.742170326720569, -22.883264740761184, 0.0 ], [ 44.728020999544768, -22.941183142974481, 0.0 ], [ 44.726664464133897, -22.945064458281752, 0.0 ], [ 44.721292631456762, -22.952338505561851, 0.0 ], [ 44.713323386958052, -22.957154675668519, 0.0 ], [ 44.703970247911478, -22.95877947405075, 0.0 ], [ 44.694657775763027, -22.956965429374211, 0.0 ], [ 44.69386119181263, -22.956460682712311, 0.0 ], [ 44.693091947860381, -22.95628420690371, 0.0 ], [ 44.689944730357844, -22.954786515855155, 0.0 ], [ 44.686476190312703, -22.956881784013319, 0.0 ], [ 44.679550801316068, -22.958417907247654, 0.0 ], [ 44.672477814938894, -22.958999287730133, 0.0 ], [ 44.671920825403106, -22.959019243802164, 0.0 ], [ 44.671788029353223, -22.959046850801808, 0.0 ], [ 44.661888919756088, -22.959598505797157, 0.0 ], [ 44.66110463279422, -22.959472387392065, 0.0 ], [ 44.660150641043195, -22.959637810268021, 0.0 ], [ 44.65551778926126, -22.958573871081143, 0.0 ], [ 44.650838720208448, -22.957821189480835, 0.0 ], [ 44.650042296536135, -22.957316223447041, 0.0 ], [ 44.649273108821383, -22.957139534943966, 0.0 ], [ 44.646837836458921, -22.95597980593055, 0.0 ], [ 44.646686138474081, -22.955998251533735, 0.0 ], [ 44.643234452419478, -22.956188980349634, 0.0 ], [ 44.633923007273722, -22.954371366701686, 0.0 ], [ 44.626071965466934, -22.949391785769386, 0.0 ], [ 44.620876818066741, -22.942008620486948, 0.0 ], [ 44.619128196546939, -22.933346171781363, 0.0 ], [ 44.620300731112366, -22.928197003278193, 0.0 ], [ 44.605097295924402, -22.929489313610411, 0.0 ], [ 44.602586851154534, -22.929582169414608, 0.0 ], [ 44.593277801163069, -22.927762181397576, 0.0 ], [ 44.585429806988259, -22.922780601415386, 0.0 ], [ 44.580237887554304, -22.915396117099871, 0.0 ], [ 44.578492187317117, -22.906733228825384, 0.0 ], [ 44.578542749686683, -22.906511455836899, 0.0 ], [ 44.578502225773988, -22.906299046532073, 0.0 ], [ 44.579641490461242, -22.872285332135593, 0.0 ], [ 44.581596520230448, -22.864097128312064, 0.0 ], [ 44.586972231965902, -22.856827796673546, 0.0 ], [ 44.587591666349788, -22.856453889783044, 0.0 ], [ 44.587965511220553, -22.855977235318328, 0.0 ], [ 44.624824527105481, -22.826116703553065, 0.0 ], [ 44.651373071652351, -22.774423008777674, 0.0 ], [ 44.649798260129899, -22.772183044670577, 0.0 ], [ 44.648049839868428, -22.763520778406779, 0.0 ], [ 44.648735422410347, -22.75844891341001, 0.0 ], [ 44.650955415673636, -22.749965531694453, 0.0 ], [ 44.65222877227815, -22.746413983194547, 0.0 ], [ 44.655584381924015, -22.741867709238168, 0.0 ], [ 44.656178360089321, -22.739875341661058, 0.0 ], [ 44.656353120676876, -22.739061907174317, 0.0 ], [ 44.666512392694301, -22.715610711866489, 0.0 ], [ 44.666658423814241, -22.715408078669608, 0.0 ], [ 44.666715704616649, -22.715155707819022, 0.0 ], [ 44.672080956961771, -22.7078832769634, 0.0 ], [ 44.680036994020064, -22.703068934598036, 0.0 ], [ 44.689372853255563, -22.701445346849805, 0.0 ], [ 44.692812654007952, -22.701707528386166, 0.0 ], [ 44.737171486447849, -22.708043576285469, 0.0 ], [ 44.74302748204142, -22.709594011714525, 0.0 ], [ 44.750869149273967, -22.714567841029325, 0.0 ], [ 44.756062852358141, -22.72194690398112, 0.0 ], [ 44.757770102507294, -22.728939018042364, 0.0 ], [ 44.765031710457443, -22.83501566462526, 0.0 ], [ 44.764892913725348, -22.835765542904156, 0.0 ], [ 44.765079339335536, -22.836684706280241, 0.0 ], [ 44.763914138526928, -22.84105340830865, 0.0 ], [ 44.763126283421826, -22.845309497832609, 0.0 ], [ 44.762582058431803, -22.846047435622463, 0.0 ], [ 44.762392010445332, -22.846759897648511, 0.0 ], [ 44.742170326720569, -22.883264740761184, 0.0 ] ] ], [ [ [ 44.821067914035119, -22.395838744491648, 0.0 ], [ 44.82132689935608, -22.395889187900192, 0.0 ], [ 44.821573769570811, -22.395848404010628, 0.0 ], [ 44.855341571939746, -22.396816861087306, 0.0 ], [ 44.864112579120494, -22.398611828892573, 0.0 ], [ 44.871941158095041, -22.40357977245656, 0.0 ], [ 44.876667831753025, -22.409958482201166, 0.0 ], [ 44.911143561136299, -22.479631261004215, 0.0 ], [ 44.911259855534816, -22.480136200530765, 0.0 ], [ 44.911606054282636, -22.480627775834069, 0.0 ], [ 44.91336864574518, -22.489005187280643, 0.0 ], [ 44.913537206969927, -22.54135646469247, 0.0 ], [ 44.913506992907173, -22.541495194418438, 0.0 ], [ 44.913536216661278, -22.541638786067985, 0.0 ], [ 44.911596777806601, -22.550265748956196, 0.0 ], [ 44.906250308494819, -22.557546482991494, 0.0 ], [ 44.900308602541806, -22.561157920152553, 0.0 ], [ 44.902991246868893, -22.56496531986182, 0.0 ], [ 44.904753343131183, -22.57362507369248, 0.0 ], [ 44.902812895782404, -22.582251894659599, 0.0 ], [ 44.897464717471742, -22.589532300697261, 0.0 ], [ 44.892763603786257, -22.592873453704573, 0.0 ], [ 44.892301182507275, -22.595685026923363, 0.0 ], [ 44.891251723924441, -22.599113060166314, 0.0 ], [ 44.891034101907863, -22.600198533719762, 0.0 ], [ 44.888140322139328, -22.607370576691672, 0.0 ], [ 44.887861269254024, -22.607768834957959, 0.0 ], [ 44.887743554252324, -22.608291769202999, 0.0 ], [ 44.882393566551045, -22.61557164468697, 0.0 ], [ 44.874449608041644, -22.620396206935514, 0.0 ], [ 44.870558703906454, -22.621078035018261, 0.0 ], [ 44.869017989866769, -22.626900218460609, 0.0 ], [ 44.867776055924445, -22.630359406987548, 0.0 ], [ 44.862424155238074, -22.637638583356434, 0.0 ], [ 44.861525803336527, -22.638183963627533, 0.0 ], [ 44.881309144098999, -22.65092165168323, 0.0 ], [ 44.884814615409361, -22.653666301095246, 0.0 ], [ 44.890013264168111, -22.661041086312476, 0.0 ], [ 44.891775734079779, -22.669700894760496, 0.0 ], [ 44.889833086399953, -22.678327454699946, 0.0 ], [ 44.886010078578188, -22.684080483681921, 0.0 ], [ 44.851046392540503, -22.722369192485175, 0.0 ], [ 44.849515967102718, -22.723895690237999, 0.0 ], [ 44.841564584662208, -22.728718517992125, 0.0 ], [ 44.835418734066664, -22.730186298813518, 0.0 ], [ 44.797984834899154, -22.734413657522271, 0.0 ], [ 44.794794546132991, -22.734577624256598, 0.0 ], [ 44.785496092544911, -22.732768964770671, 0.0 ], [ 44.777652116317689, -22.72779682918403, 0.0 ], [ 44.772457028624672, -22.72041846263096, 0.0 ], [ 44.770750560130807, -22.713447249383677, 0.0 ], [ 44.768930764134353, -22.687255100925142, 0.0 ], [ 44.769070500522602, -22.686497044859049, 0.0 ], [ 44.768881691525642, -22.685565279600514, 0.0 ], [ 44.770832018138918, -22.676940251527199, 0.0 ], [ 44.771801425324625, -22.675073457556852, 0.0 ], [ 44.78618043565799, -22.650346707752515, 0.0 ], [ 44.790567928843238, -22.644937050343437, 0.0 ], [ 44.793197433659287, -22.642864002551846, 0.0 ], [ 44.795906411616691, -22.641018307120195, 0.0 ], [ 44.790764140522676, -22.639900220226004, 0.0 ], [ 44.782925238806044, -22.634928338196012, 0.0 ], [ 44.780423911966679, -22.631373816549175, 0.0 ], [ 44.776647783422, -22.628978365943023, 0.0 ], [ 44.77145642975016, -22.621599925712097, 0.0 ], [ 44.770171430990075, -22.617596743130086, 0.0 ], [ 44.767492917918311, -22.605137695980815, 0.0 ], [ 44.7670237376242, -22.600479751269738, 0.0 ], [ 44.768972918341902, -22.591854664842899, 0.0 ], [ 44.77184896586769, -22.58794697694157, 0.0 ], [ 44.789438717280511, -22.506247114802925, 0.0 ], [ 44.786252175795056, -22.500655691351461, 0.0 ], [ 44.782463238244681, -22.499785259583795, 0.0 ], [ 44.778305297411606, -22.497670818810388, 0.0 ], [ 44.769914479903932, -22.492354419007754, 0.0 ], [ 44.766242027807536, -22.489496392108578, 0.0 ], [ 44.761056380445822, -22.482117576692268, 0.0 ], [ 44.759304389395687, -22.473456257195643, 0.0 ], [ 44.761252160498969, -22.464831147666938, 0.0 ], [ 44.761499509302759, -22.464494803406783, 0.0 ], [ 44.761588495097897, -22.464131497357567, 0.0 ], [ 44.762978587742232, -22.461366617448768, 0.0 ], [ 44.763080663415536, -22.459669383656522, 0.0 ], [ 44.763865972937566, -22.452478913701032, 0.0 ], [ 44.765690287142618, -22.445897579927337, 0.0 ], [ 44.768417078173705, -22.441439123398659, 0.0 ], [ 44.771987883922655, -22.436893209363049, 0.0 ], [ 44.772408475531869, -22.436441319887162, 0.0 ], [ 44.772588546235042, -22.43616034864877, 0.0 ], [ 44.777581365773536, -22.43031935393423, 0.0 ], [ 44.779603029153058, -22.428234434494037, 0.0 ], [ 44.785194198538534, -22.423143128674528, 0.0 ], [ 44.787495445285622, -22.420153032511319, 0.0 ], [ 44.79080753610895, -22.41316789200576, 0.0 ], [ 44.796036576044841, -22.406143570866632, 0.0 ], [ 44.801623521279701, -22.402339053512105, 0.0 ], [ 44.809406938734057, -22.398486759340589, 0.0 ], [ 44.811754172822759, -22.397470288222607, 0.0 ], [ 44.821067914035119, -22.395838744491648, 0.0 ] ] ] ] } }"><?= $geolocalisation[0]->geojson ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="card card-warning">
                        <div class="card-header">
                            <h3 class="card-title">Potentialités</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label>Potentialités de l'aire protégée</label>
                                <textarea class="textarea" placeholder="Place some text here" name="potentialites"
                                style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                                <?= $ap['potentialites'] ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="card card-danger">
                        <div class="card-header">
                            <h3 class="card-title">Menaces</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label>Les menaces</label>
                                <textarea class="textarea" placeholder="Place some text here" name="menaces"
                                style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                                <?= $ap['menaces'] ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label>Image</label>
                    <div class="custom-file">
                    <input type="file" class="custom-file-input" id="exampleInputFile" name="ap_image" multiple>
                    <label class="custom-file-label" for="exampleInputFile">Importer une image</label>
                </div>
            </div>
          </div>
          <div class="col-md-12">
            <button type="submit" class="btn btn-block btn-success btn-lg">Modifier l'aire protégée</button>
          </div>
            </form>
        </div>
        <!-- /.card -->

        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    

<?= $this->endSection('') ?>