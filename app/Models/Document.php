<?php

namespace App\Models;

use CodeIgniter\Model;

class DocumentModel extends Model
{
    protected $table = 'Documents';
    protected $primaryKey = 'id';
    protected $allowedFields = ['titre', 'lien'];
    protected $rules = [
        'titre' => [
            'rules' => 'required|is_unique[documents.titre]',
            'errors' => [
                'required' => "Veuillez donner un titre au document",
                'is_unique' => "Ce document existe déjà"
            ]
        ]
    ];
}
