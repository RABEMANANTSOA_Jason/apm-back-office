<?php

namespace App\Models\custom;

use CodeIgniter\Database\ConnectionInterface;

class PopulationCustomModel
{
    protected $db;

    public function __construct(ConnectionInterface &$db)
    {
        $this->db =& $db;
    }

    public function getPopulationAP($id){
        $builder = $this->db->table('Population');
        $builder->where(['Population.id_aire_protegee' => $id]);
        $result = $builder->get()->getResult();
        return $result;
    }

    public function getDetailPopulation($id){
        $builder = $this->db->table('Population');
        $builder->where(['Population.id_population' => $id]);
        $builder->join('Aires_Protegees','Aires_Protegees.id_ap = Population.id_aire_protegee');
        $result = $builder->get()->getResult();
        return $result;
    }
}
