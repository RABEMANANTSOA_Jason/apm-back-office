<?php

namespace App\Models;

use CodeIgniter\Model;

class FokontanyModel extends Model
{
    protected $table = 'Fokontany';
    protected $primaryKey = 'id';
    protected $allowedFields = ['fokontany', 'id_commune'];
    protected $rules = [
        'commune' => [
            'rules' => 'required|is_unique[fokontany.fokontany]',
            'errors' => [
                'required' => "Veuillez donner un nom de 'fokontany'",
                'is_unique' => "Ce 'fokontany' existe déjà"
            ]
        ],
        'id_commune' => [
            'rules' => 'required',
            'errors' => [
                'required' => "Veuillez référencer une commune"
            ]
        ]
    ];

    public function getFokontanyAll(){
        return $this->findAll();
    }
}
